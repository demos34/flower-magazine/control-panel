@extends('layouts.app')

@section('title')
    <title>{{ config('app.name', 'Laravel') }} | Select type</title>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 mx-auto">
                <div class="card my-3 mx-auto">
                    <div class="card-header">
                        <h4 class="text-center">
                            <strong>Типове категории:</strong>
                        </h4>
                    </div>
                    <div class="card-body">
                        @if($types->count() > 0)
                            <form action="/products/create" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="card-body">
                                    <label for="type_id">
                                        Select type:
                                    </label>
                                    <select class="form-control" name="type_id" id="type_id">
                                        @foreach($types as $type)
                                            <option value="{{$type->id}}">{{$type->bg_name}} / {{$type->eng_name}}</option>
                                        @endforeach
                                    </select>
                                    @error('type_id')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-outline-success">Select</button>
                                </div>
                            </form>
                        @else
                            <strong>
                                No types set yet!
                            </strong>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
