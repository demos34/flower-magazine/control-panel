<?php

namespace App\Repositories;

use App\Models\Tags\IndexDescription;
use App\Models\Tags\IndexTag;
use App\Models\Tags\Keyword;
use App\Models\Tags\Tag;
use Illuminate\Http\Request;

interface TagsRepositoryInterface
{
    public function getAllTags();

    public function getAllIndexTags();

    public function getIndexDescription();

    public function storeTag(Request $request);

    public function storeIndexTag(Request $request);

    public function storeIndexDescription(Request $request);

    public function updateTag(Request $request, Tag $tag);

    public function updateIndexTag(Request $request, IndexTag $indexTag);

    public function updateDescription(Request $request, IndexDescription $indexDescription);

    public function deleteTag(Tag $tag);

    public function deleteIndexTag(IndexTag $indexTag);

    public function getAllKeywords();

    public function storeKeyword(Request $request);

    public function updateKeyword(Request $request, Keyword $keyword);

    public function deleteKeyword(Keyword $keyword);
}
