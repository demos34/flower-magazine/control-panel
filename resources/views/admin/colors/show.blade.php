@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div>
                    <a href="/colors/">
                        <button class="btn btn-outline-dark">Back</button>
                    </a>
                </div>
                <div class="card my-3 mx-auto">
                    <div class="card-header">
                        <h4 align="center">
                            <strong>Цвят:</strong>
                        </h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Color</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td>
                                            {{$color->id}}
                                        </td>
                                        <td style="color: {{$color->eng_color}}">
                                            <strong>
                                                {{$color->bg_color}} / {{$color->eng_color}}
                                            </strong>
                                        </td>
                                        <td>
                                            <img src="{{$color->image}}" style="height: 15px; width: 15px" class="rounded-circle" alt="">
                                        </td>
                                        <td>
                                            <form action="/colors/{{$color->id}}" method="POST" class="float-left">
                                                @csrf
                                                @method('DELETE')
                                                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-outline-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-3">
                <div class="well">
                    <h4 align="center">
                        <label>
                            Промени цвят <strong style="color: {{$color->eng_color}}">{{$color->bg_color}} / {{$color->eng_color}}</strong>
                        </label>
                    </h4>
                    <form action="/colors/{{$color->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="card">
                            <div class="card-header">
                                <label for="bg_color">
                                    BG Color:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="bg_color"
                                           type="text"
                                           class="form-control @error('bg_color') is-invalid @enderror"
                                           name="bg_color"
                                           value=@if(old('bg_color'))"{{ old('bg_color') }}"@else"{{$color->bg_color}}"@endif
                                    required autocomplete="bg_color" autofocus>
                                    @error('bg_color')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <div class="card-header">
                                <label for="eng_color">
                                    ENG Color:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="eng_color"
                                           type="text"
                                           class="form-control @error('eng_color') is-invalid @enderror"
                                           name="eng_color"
                                           value=@if(old('eng_color'))"{{ old('eng_color') }}"@else"{{$color->eng_color}}"@endif
                                    required autocomplete="eng_color" autofocus>
                                    @error('eng_color')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">
                                    Image <img src="{{$color->image}}" style="height: 15px; width: 15px" class="rounded-circle" alt="">
                                </label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control-file" id="image" name="image">
                                    @if($errors->has('image'))
                                        <strong>{{ $errors->first('image') }}</strong>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

