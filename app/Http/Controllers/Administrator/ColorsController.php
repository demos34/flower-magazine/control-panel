<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Administrator\Color;
use App\Repositories\ColorsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class ColorsController extends Controller
{
    private $trafficRepository;
    /**
     * @var ColorsRepositoryInterface
     */
    private $colorsRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                ColorsRepositoryInterface  $colorsRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->middleware('auth');
        $this->colorsRepository = $colorsRepository;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $colors = $this->colorsRepository->getAll();

        return view('admin.colors.index')->with('colors', $colors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $this->colorsRepository->store($request);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $color
     * @return \Illuminate\Http\Response
     */
    public function show(Color $color)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return view('admin.colors.show')->with('color', $color);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $color
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Color $color)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $update = $this->colorsRepository->update($request, $color);
        return $this->show($color);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $color
     * @return \Illuminate\Http\Response
     */
    public function destroy(Color $color)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $delete = $this->colorsRepository->delete($color);
        return $this->index();
    }
}
