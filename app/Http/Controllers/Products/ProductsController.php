<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Models\Administrator\Color;
use App\Models\Products\Image;
use App\Models\Products\Product;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\ProductsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;

    /**
     * @var ProductsRepositoryInterface
     */
    private $productsRepository;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                ProductsRepositoryInterface $productsRepository,
                                CategoryRepositoryInterface $categoryRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->productsRepository = $productsRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $product = Product::find(1);

        $images = Image::all();
        dd($images);
    }

    /**
     * Show the form for creating a new resource.
     * @param  \Illuminate\Http\Request  $request
     *
     */
    public function create(Request $request)
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        $categories = $this->productsRepository->getAllCategories($request);
        $tags = $this->categoryRepository->getAllTags();
        $keywords = $this->categoryRepository->getAllKeywords();
        $colors = $this->productsRepository->allColors();

        return view('products.create')->with(
            [
                'categories' => $categories,
                'tags' => $tags,
                'keywords' => $keywords,
                'colors' => $colors,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $product = $this->productsRepository->store($request);

        return redirect('/products/' . $product->id)->with('success', 'Item is successfully added to market');

    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     */
    public function show(Product $product)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $product
     */
    public function edit(Product $product)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);

        $categories = $this->productsRepository->getAllCategoriesFromProductId($product);
        $tags = $this->categoryRepository->getAllTags();
        $keywords = $this->categoryRepository->getAllKeywords();
        $colors = $this->productsRepository->allColors();

        return view('products.edit')->with(
            [
                'product' => $product,
                'categories' => $categories,
                'tags' => $tags,
                'keywords' => $keywords,
                'colors' => $colors,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $this->productsRepository->update($request, $product);

        return $this->edit($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $product
     */
    public function destroy(Product $product)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $id = $this->productsRepository->delete($product);

        return redirect('/category/show/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $image
     */
    public function destroyImage(Image $image)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $product = $this->productsRepository->deleteSecondaryImagesFromController($image);

        return $this->edit($product);
    }
}
