@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div>
                    <a href="/warehouse/">
                        <button class="btn btn-outline-dark">Back</button>
                    </a>
                </div>
                <div class="card my-3 mx-auto">
                    <div class="py-1">
                        <div class="card-header">
                            <h4 class="text-center">
                                <strong>Product {{$warehouse->product->eng_name}} / {{$warehouse->product->bg_name}}</strong>
                            </h4>
                        </div>
                        <div class="card-body">
                            <table class="table table-borderless text-center">
                                <thead>
                                <tr>
                                    <th>Warehouse id</th>
                                    <th>Product id</th>
                                    <th>Type</th>
                                    <th>Category</th>
                                    <th>Qty</th>
                                    <th>Dimension</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        {{$warehouse->id}}
                                    </td>
                                    <td>
                                        {{$warehouse->product->id}}
                                    </td>
                                    <td>
                                        {{$warehouse->type->eng_name}} / {{$warehouse->type->bg_name}}
                                    </td>
                                    <td>
                                        {{$warehouse->category->eng_name}} / {{$warehouse->category->bg_name}}
                                    </td>
                                    <td>
                                        {{$warehouse->quantity}}
                                    </td>
                                    <td>
                                        {{$warehouse->metric}}
                                    </td>
                                    <td>
                                        <form action="/warehouse/{{$warehouse->id}}" method="POST" class="float-left">
                                            @csrf
                                            @method('DELETE')
                                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-outline-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card my-3 mx-auto">
                    <div class="py-1">
                        <div class="card-header">
                            <h4 class="text-center">
                                <strong>Turn over (Movement)</strong>
                            </h4>
                        </div>
                        <div class="card-body">
                            <table class="table table-borderless text-center">
                                <thead>
                                <tr>
                                    <th>+/-</th>
                                    <th>Before (qty)</th>
                                    <th>By how much</th>
                                    <th>After (qty)</th>
                                    <th>At date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($warehouse->movements as $movement)
                                    <tr>
                                        <td>
                                            @if($movement->to_where == 'subtract')
                                                <strong>-</strong>
                                            @else
                                                <strong>+</strong>
                                            @endif
                                        </td>
                                        <td>
                                            {{implode(',', $movement->details()->get()->pluck('old')->toArray())}}
                                        </td>
                                        <td>
                                            {{implode(',', $movement->details()->get()->pluck('by_how_much')->toArray())}}
                                        </td>
                                        <td>{{implode(',', $movement->details()->get()->pluck('new')->toArray())}}</td>
                                        <td>{{implode(',', $movement->details()->get()->pluck('created_at')->toArray())}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-3">
                <div class="well">
                    <h4 class="text-center">
                        <label>
                            Add or Subtract quantity of product
                        </label>
                    </h4>
                    <form action="/warehouse/qty/{{$warehouse->id}}" method="POST" class="text-center">
                        @csrf
                        @method('PATCH')
                        <div class="card">
                            <div class="card-header">
                                <label for="quantity">
                                    Quantity (in warehouse are - {{$warehouse->quantity}}):
                                </label>
                            </div>
                            <div class="card-body">
                                <label for="quantity">
                                    Select by how many:
                                </label>
                                    <input id="quantity"
                                           type="number"
                                           min="0.01"
                                           step="0.01"
                                           class="form-control @error('quantity') is-invalid @enderror text-center"
                                           name="quantity"
                                           value=@if(old('quantity'))"{{ old('quantity') }}"@else"{{$warehouse->quantity}}"@endif
                                    required autocomplete="bg_color" autofocus>
                                    @error('quantity')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <div class="card-body text-center">
                                <label for="move_to">
                                    Add or subtract:
                                </label>
                                <select class="form-control align-content-center" name="move_to" id="move_to">
                                    <option value="add" class="text-center">
                                        Add (+)
                                    </option>
                                    <option value="subtract" class="text-center">
                                        Subtract (-)
                                    </option>
                                </select>
                                @error('color')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Go!
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

