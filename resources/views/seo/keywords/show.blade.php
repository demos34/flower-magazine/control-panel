@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div>
                    <a href="/seo/">
                        <button class="btn btn-outline-dark">Back</button>
                    </a>
                    <form method="post" action="/keywords/{{$keyword->id}}" class="float-right">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-outline-danger">Delete</button>
                    </form>
                </div>
                <div class="card my-3 mx-auto">
                    <div class="card-header">
                        <h4 align="center">
                            Ключова дума <strong style="color: blue">{{$keyword->bg_keyword}}
                                / {{$keyword->eng_keyword}}</strong>, използвана в следните продукти:
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="card-group">
                            <div class="mx-5">#</div>
                            <div class="mx-5">Product type</div>
                            <div class="mx-auto">Product</div>
                        </div>
                        <hr>
                        <div class="card-group">
                            <div class="mx-5">ID</div>
                            <div class="mx-4">
                                <a href="#">Опаковки / Цветя</a>
                            </div>
                            <div class="mx-auto">
                                <a href="#">Продукт(опаковка/цвете)</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-3">
                <div class="well">
                    <h4 align="center">
                        <label>
                            Добави нов мета-таг
                        </label>
                    </h4>
                    <form action="/keywords/{{$keyword->id}}/" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="card">
                            <div class="card-header">
                                <label for="bg_keyword">
                                    Bg Keyword:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="bg_keyword"
                                           type="text"
                                           class="form-control @error('bg_keyword') is-invalid @enderror"
                                           name="bg_keyword"
                                           value=@if(old('bg_keyword'))"{{ old('bg_keyword') }}"@else
                                        "{{$keyword->bg_keyword}}"@endif
                                    required autocomplete="bg_keyword" autofocus>
                                    @error('bg_keyword')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <div class="card-header">
                                <label for="eng_keyword">
                                    Eng Keyword:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="eng_keyword"
                                           type="text"
                                           class="form-control @error('eng_keyword') is-invalid @enderror"
                                           name="eng_keyword"
                                           value=@if(old('eng_keyword'))"{{ old('eng_keyword') }}"@else
                                        "{{$keyword->eng_keyword}}"@endif
                                    required autocomplete="eng_keyword" autofocus>
                                    @error('eng_keyword')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
