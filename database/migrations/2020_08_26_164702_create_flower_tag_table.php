<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlowerTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flower_tag', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('flower_id');
            $table->unsignedBigInteger('tag_id');
            $table->timestamps();

            $table->foreign('flower_id')->references('id')->on('flowers')->onDelete('cascade');
            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flower_tag');
    }
}
