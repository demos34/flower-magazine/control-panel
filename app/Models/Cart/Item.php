<?php

namespace App\Models\Cart;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo('App\Models\Products\Product');
    }

    public function carts()
    {
        return $this->belongsToMany('App\Models\Cart\Cart');
    }
}
