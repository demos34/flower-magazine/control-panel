<?php
namespace App\Repositories;

use App\Models\Administrator\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryInterface
{
    public function all()
    {
        return User::all()->sortBy('username');
    }

    public function allRoles()
    {
        return Role::all();
    }

    public function update(Request $request, User $user)
    {
        $validated = $this->prepareForUpdate($request, $user);
        $user->update(
            [
                'username' => $validated['username'],
                'name' => $validated['name'],
                'email' => $validated['email'],
            ]
        );
        $user->roles()->sync($validated['role']);
    }

    private function prepareForUpdate(Request $request, User $user)
    {
        if($request->username == $user->username && $request->email == $user->email){
            $validate = $this->validateSameInputs($request);
        } elseif ($request->username != $user->username && $request->email == $user->email){
            $validate = $this->validateUsername($request);
        } elseif ($request->username == $user->username && $request->email != $user->email){
            $validate = $this->validateEmail($request);
        } else {
            $validate = $this->validateAll($request);
        }
        return $validate;
    }

    private function validateAll(Request $request)
    {
        return $request->validate(
            [
                'username' => 'required|min:3|max:100|unique:users',
                'name' => 'required|min:5|max:100',
                'email' => 'required|min:5|max:100|unique:users',
                'role' => 'required|exists:roles,id',
            ]
        );
    }

    private function validateSameInputs(Request $request)
    {
        return $request->validate(
            [
                'username' => 'required',
                'name' => 'required|min:5|max:100',
                'email' => 'required',
                'role' => 'required|exists:roles,id',
            ]
        );
    }

    private function validateUsername(Request $request)
    {
        return $request->validate(
            [
                'username' => 'required|min:3|max:100|unique:users',
                'name' => 'required|min:5|max:100',
                'email' => 'required',
                'role' => 'required|exists:roles,id',
            ]
        );
    }

    private function validateEmail(Request $request)
    {
        return $request->validate(
            [
                'username' => 'required',
                'name' => 'required|min:5|max:100',
                'email' => 'required|min:5|max:100|unique:users',
                'role' => 'required|exists:roles,id',
            ]
        );
    }

    public function delete(User $user)
    {
        return $user->delete();
    }

    public function changePassword(Request $request, User $user)
    {
        $validated = $this->passwordValidation($request);
        $password = $validated['password'];
        $hashedPassword = Hash::make($password);

        return $user->update(
            [
                'password' => $hashedPassword,
            ]
        );
    }

    private function passwordValidation(Request $request)
    {
        return $request->validate(
            [
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]
        );
    }

    public function register(Request $request)
    {
        $validated = $this->validateRegister($request);

        $user = User::create([
            'username' => $validated['username'],
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => Hash::make($validated['password']),
        ]);

        $user->roles()->attach('3');

        return $user;
    }

    private function validateRegister(Request $request)
    {
        return $request->validate(
            [
                'username' => ['required', 'string', 'max:255', 'unique:users'],
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]
        );
    }
}
