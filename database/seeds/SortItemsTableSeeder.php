<?php

use App\Models\Administrator\SortItem;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SortItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        SortItem::truncate();
        DB::table('sort_items')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        SortItem::create(
            [
                'bg_sort' => 'Цена (възходящо)',
                'eng_sort' => 'Price (increment)',
                'column' => 'price',
                'to_where' => 'asc',
            ]
        );

        SortItem::create(
            [
                'bg_sort' => 'Цена (низходящо)',
                'eng_sort' => 'Price (decrement)',
                'column' => 'price',
                'to_where' => 'desc',
            ]
        );

        SortItem::create(
            [
                'bg_sort' => 'Дата (от стари към нови)',
                'eng_sort' => 'Date (from oldest to newest)',
                'column' => 'created_at',
                'to_where' => 'desc',
            ]
        );

        SortItem::create(
            [
                'bg_sort' => 'Дата (от нови към стари)',
                'eng_sort' => 'Date (from newest to oldest)',
                'column' => 'created_at',
                'to_where' => 'asc',
            ]
        );
    }
}
