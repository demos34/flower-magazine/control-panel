<?php

namespace App\Repositories;

use App\Models\Types\Type;
use Illuminate\Http\Request;

interface TypesRepositoryInterface
{
    public function getAll();

    public function storeType(Request $request);

    public function updateType(Request $request, Type $type);

    public function deleteType(Type $type);

    public function animStore(Request $request, Type $type);

    public function animUpdate(Request $request, Type $type);
}
