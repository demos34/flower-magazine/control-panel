<?php

namespace App\Repositories;

use App\Models\Products\Product;
use Illuminate\Http\Request;

interface CartRepositoryInterface
{
    public function getCartContent();

    public function getTotal();

    public function addCartContent(Request $request, Product $product);

    public function deleteFromCart($rowId);

    public function updateCart(Request $request);

    public function purchase(Request $request);
}
