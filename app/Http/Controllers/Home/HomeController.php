<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{

    private $trafficRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository)
    {
        $this->trafficRepository = $trafficRepository;
    }


    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);
        return view('home.index');
    }

    public function about()
    {
        return 'about us';
    }

    public function contacts()
    {
        dd(App::getLocale());
    }

}
