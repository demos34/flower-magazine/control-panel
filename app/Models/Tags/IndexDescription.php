<?php

namespace App\Models\Tags;

use Illuminate\Database\Eloquent\Model;

class IndexDescription extends Model
{
    protected $guarded = [];
}
