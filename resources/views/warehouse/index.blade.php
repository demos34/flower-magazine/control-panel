@extends('layouts.app')

@section('title')
    <title>{{ config('app.name', 'Laravel') }} | Warehouse</title>
@endsection

@section('custom-css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container my-auto">
        <div class="row text-center border-bottom my-2 p-2">
            <div class="col-lg-4">
                <div class="custom-dropdown">
                    <span>View all of selected type</span>
                    <div class="custom-dropdown-content">
                        @foreach($types as $type)
                            <a href="/warehouse/all/type/asc/{{$type->id}}" style="border-bottom: 1px solid black">
                                {{$type->eng_name}} Increment
                            </a>
                            <br>
                            <a href="/warehouse/all/type/desc/{{$type->id}}" style="border-bottom: 1px solid black">
                                {{$type->eng_name}} Decrement
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="custom-dropdown">
                    <span>View all of selected category:</span>
                    <div class="custom-dropdown-content">
                        @foreach($categories as $category)
                            <a href="/warehouse/all/category/asc/{{$category->id}}" style="border-bottom: 1px solid dimgray">
                                {{$category->eng_name}} Increment
                            </a>
                            <br>
                            <a href="/warehouse/all/category/desc/{{$category->id}}" style="border-bottom: 1px solid black">
                                {{$category->eng_name}} Decrement
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="custom-dropdown">
                    <span>View by:</span>
                    <div class="custom-dropdown-content">
                        <a href="/warehouse/by/typeasc" style="border-bottom: 1px solid black">
                            Types Increment
                        </a>
                        <br>
                        <a href="/warehouse/by/typedesc" style="border-bottom: 1px solid black">
                            Types Decrement
                        </a>
                        <br>
                        <a href="/warehouse/by/categoryasc" style="border-bottom: 1px solid black">
                            Categories Increment
                        </a>
                        <br>
                        <a href="/warehouse/by/categorydesc" style="border-bottom: 1px solid black">
                            Categories Decrement
                        </a>
                        <br>
                        <a href="/warehouse/by/productasc" style="border-bottom: 1px solid black">
                            Products Increment
                        </a>
                        <a href="/warehouse/by/productdesc" style="border-bottom: 1px solid black">
                            Products Decrement
                        </a>
                        <br>
                        <a href="/warehouse/by/nameasc" style="border-bottom: 1px solid black">
                            View by product name Increment
                        </a>
                        <br>
                        <a href="/warehouse/by/namedesc" style="border-bottom: 1px solid black">
                            View by product name Decrement
                        </a>
                        <br>
                        <a href="/warehouse/by/quantityasc" style="border-bottom: 1px solid black">
                            View by quantity Increment
                        </a>
                        <br>
                        <a href="/warehouse/by/quantitydesc" style="border-bottom: 1px solid black">
                            View by quantity Decrement
                        </a>
                        <br>
                        <a href="/warehouse/by/allasc" style="border-bottom: 1px solid black">
                            View all Increment
                        </a>
                        <br>
                        <a href="/warehouse/by/alldesc" style="border-bottom: 1px solid black">
                            View all Decrement
                        </a>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-12 my-auto">
                <div class="card my-3 mx-auto">
                    <div class="card-header text-center">
                        <strong>Your warehouse</strong>
                    </div>
                    <div class="card-body">
                        @if($warehouses->count() > 0)
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Type</th>
                                <th>Category</th>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($warehouses as $warehouse)
                                    <tr>
                                        <td>
                                            {{$warehouse->id}}
                                        </td>
                                        <td>
                                            <strong>
                                                {{$warehouse->type->bg_name}} / {{$warehouse->type->eng_name}}
                                            </strong>
                                        </td>
                                        <td>
                                            <strong>
                                                {{$warehouse->category->bg_name}} / {{$warehouse->category->eng_name}}
                                            </strong>
                                        </td>
                                        <td>
                                            <strong>
                                                {{$warehouse->product->bg_name}} / {{$warehouse->product->eng_name}}
                                            </strong>
                                        </td>
                                        <td>
                                            <strong>
                                                {{$warehouse->quantity}} {{$warehouse->metric}}
                                            </strong>
                                        </td>
                                        <td>
                                            <a href="/warehouse/{{$warehouse->id}}">
                                                <button class="btn btn-outline-success float-left">View</button>
                                            </a>
                                            <form action="/warehouse/{{$warehouse->id}}" method="POST" class="float-right">
                                                @csrf
                                                @method('DELETE')
                                                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-outline-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                            <strong style="color: red; font-size: 2em"> No products in warehouse yet! </strong>
                        @endif
                        {{$warehouses->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

