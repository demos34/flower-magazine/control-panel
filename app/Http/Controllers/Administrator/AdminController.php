<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Administrator\Message;
use App\Models\Administrator\SortItem;
use App\Models\Cart\Cart;
use App\Repositories\AdminRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    private $trafficRepository;
    /**
     * @var AdminRepositoryInterface
     */
    private $adminRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository, AdminRepositoryInterface $adminRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->middleware('auth');
        $this->adminRepository = $adminRepository;
    }


    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $local = App::getLocale();

        if(App::isLocal('en')){
            $local = 'eng_';
        } else {
            $local = 'bg';
        }

        return view('admin.admin.index');
    }

    public function sort()
    {
        $action = 'sort';
        $this->trafficRepository->getRemoteAddress($action);

        $sortBy = $this->adminRepository->viewAllSortBy();

        return view('admin.sort.index')->with('sortBy', $sortBy);
    }

    public function sortStore(Request $request)
    {
        $action = 'sortStore';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->storeSortBy($request);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  $sortItem
     * @return Response
     */
    public function sortShow(SortItem $sortItem)
    {
        $action = 'sortShow';
        $this->trafficRepository->getRemoteAddress($action);

        return view('admin.sort.show')->with('sortBy', $sortItem);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param SortItem $sortItem
     * @return Response
     */

    public function sortUpdate(Request $request, SortItem $sortItem)
    {
        $action = 'sortUpdate';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->updateSortBy($request, $sortItem);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  $sortItem
     * @return Response
     */
    public function sortDestroy(SortItem $sortItem)
    {
        $action = 'sortDelete';
        $this->trafficRepository->getRemoteAddress($action);

        $this->adminRepository->deleteSortBy($sortItem);
        return $this->sort();
    }

    public function messages()
    {
        $action = 'messages';
        $this->trafficRepository->getRemoteAddress($action);

        $messages = $this->adminRepository->getAllUnreadMessages();
        return view('admin.messages.index')->with('messages', $messages);
    }

    public function messagesDelete(Message $message)
    {
        $action = 'messagesDelete';
        $this->trafficRepository->getRemoteAddress($action);

        $this->adminRepository->deleteMessage($message);
        return redirect('/messages')->with('success', 'The message is successfully deleted!');
    }

    public function cart()
    {
        $action = 'cart';
        $this->trafficRepository->getRemoteAddress($action);

        $carts = $this->adminRepository->getAllAdminCart();
        return view('admin.cart.index')->with('carts', $carts);
    }

    public function cartShow(Cart $cart)
    {
        $action = 'cartShow';
        $this->trafficRepository->getRemoteAddress($action);

        return view('admin.cart.show')->with('cart', $cart);
    }

    public function cartSort($slug)
    {
        $action = 'cartSort';
        $this->trafficRepository->getRemoteAddress($action);

        $carts = $this->adminRepository->getAllAdminCartSorted($slug);
        return view('admin.cart.index')->with('carts', $carts);
    }

    public function cartChangeStatus($status, Cart $cart)
    {
        $action = 'cartChangeStatus';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->changeStatus($status, $cart);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function cartDelete(Cart $cart)
    {
        $action = 'cartChangeStatus';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->deleteCart($cart);
        return redirect('/administrator/cart')->with($msg['session'], $msg['message']);
    }


}
