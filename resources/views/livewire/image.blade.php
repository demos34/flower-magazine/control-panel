<div>
    <div>
        <span class="form-control">
            Secondary images -> <strong><span wire:click="increment">Add more</span></strong>
        </span>
    </div>
    <div class="row justify-content-center">
        @foreach($images as $image)
            <div class="col-md-5 justify-content-center my-1">
                <input type="file" name="secondary_images[]"> <strong><span wire:click="remove({{$loop->index}})">Remove</span></strong>
            </div>
        @endforeach
    </div>
    <h1 style="color: red">
    </h1>
</div>
