@extends('layouts.app')

@section('custom-css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div>
                    <a href="/sort/">
                        <button class="btn btn-outline-dark">Back</button>
                    </a>
                </div>
                <div class="card my-3 mx-auto">
                    <div class="card-header">
                        <h4 class="text-center">
                            <strong>Sort items by:</strong>
                        </h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Sort items by</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td>
                                            {{$sortBy->id}}
                                        </td>
                                        <td>
                                            <strong>
                                                {{$sortBy->bg_sort}} / {{$sortBy->eng_sort}}
                                            </strong>
                                        </td>
                                        <td>
                                            <img src="{{$sortBy->image}}" style="height: 15px; width: 15px" class="rounded-circle" alt="">
                                        </td>
                                        <td>
                                            <form action="/sort/{{$sortBy->id}}" method="POST" class="float-left">
                                                @csrf
                                                @method('DELETE')
                                                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-outline-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-3">
                <div class="well">
                    <h4 align="center">
                        <label>
                            Edit field <strong>{{$sortBy->bg_sort}} / {{$sortBy->eng_sort}}</strong>
                        </label>
                    </h4>
                    <form action="/sort/{{$sortBy->id}}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="card">
                            <div class="card-header">
                                <label for="bg_sort">
                                    BG Sort by:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="bg_sort"
                                           type="text"
                                           class="form-control @error('bg_sort') is-invalid @enderror"
                                           name="bg_sort"
                                           value=@if(old('bg_sort'))"{{ old('bg_sort') }}"@else"{{$sortBy->bg_sort}}"@endif
                                    required autocomplete="bg_sort" autofocus>
                                    @error('bg_sort')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <div class="card-header">
                                <label for="eng_sort">
                                    ENG Sort by:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="eng_sort"
                                           type="text"
                                           class="form-control @error('eng_sort') is-invalid @enderror"
                                           name="eng_sort"
                                           value=@if(old('eng_sort'))"{{ old('eng_sort') }}"@else"{{$sortBy->eng_sort}}"@endif
                                    required autocomplete="eng_color" autofocus>
                                    @error('eng_sort')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                        </div>
                        <div class="card-header">
                            <label for="column">
                                Wich column will be sorted:
                            </label>
                        </div>
                        <div class="card-body">
                            <input id="column"
                                   type="text"
                                   class="form-control @error('column') is-invalid @enderror"
                                   name="column"
                                   value=@if(old('column'))"{{ old('column') }}"@else"{{$sortBy->column}}"@endif
                                   required autocomplete="column" autofocus>
                            @error('column')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="card-header">
                            <label for="to_where">
                                Sort to where (Increment or Decrement):
                            </label>
                        </div>
                        <div class="card-body">
                            <select class="form-control" name="to_where" id="to_where">
                                <option value="asc">
                                    Increment
                                </option>
                                <option value="desc">
                                    Decrement
                                </option>
                            </select>
                            @error('to_where')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

