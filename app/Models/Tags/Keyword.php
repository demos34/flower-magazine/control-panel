<?php

namespace App\Models\Tags;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $guarded = [];

    public function categories()
    {
        return $this->belongsToMany('App\Models\Administrator\Category');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Products\Product');
    }

    public function types()
    {
        return $this->belongsToMany('App\Models\Types\Type');
    }
}
