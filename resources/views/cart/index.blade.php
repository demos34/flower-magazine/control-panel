@extends('layouts.app')

@section('title')
    <title>{{ config('app.name', 'Laravel') }} | Show cart</title>
@endsection

@section('content')
    <div class="container mx-auto">
        <div class="row justify-content-center">
            <div class="col-md-10" style="background-color: antiquewhite">
                <div class="d-flex justify-content-center">
                    <div class="btn justify-content-center" style="background-color: #e67cb9">
                        <h2>ВАШАТА КОЛИЧКА</h2>
                    </div>
                </div>
                    <form action="/cart" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row justify-content-center" style="background-color: yellowgreen">
                            @foreach($cart as $item)
                                <div class="col-md-2">
                                    <img src="{{$item->options->image}}" alt="" style="width: 10em; height: 10em">
                                </div>
                                <div class="col-md-3 my-auto">
                                    <h4>{{$item->name}}</h4>
                                </div>
                                <div class="col-md-2 my-auto product-cart">
                                    <div class="product">
                                        <button type="button" class="qtyMinus" style="border: none; background-color: white"
                                                onclick="minus({{$item->id}})">-
                                        </button>
                                        <input min="1" id="input-quantity-{{$item->id}}"
                                               class="mx-auto text-center input-text qtyField"
                                               name="quantity-for-{{$item->id}}" value="{{$item->qty}}" style="width: 60%">
                                        <button type="button" class="qtyPlus" style="border: none; background-color: white"
                                                onclick="plus({{$item->id}})">+
                                        </button>
                                        <script type="text/javascript">
                                        </script>
                                    </div>
                                </div>
                                <div class="col-md-2 my-auto">
                                    <input disabled hidden value="{{$item->price}}" id="input-price-{{$item->id}}">
                                    <strong class="price" id="input-total-price-{{$item->id}}">{{$item->options->full_price}}</strong> лв. / общо
                                </div>
                                <div class="col-md-3 my-auto">
                                    <a href="" class="float-left">
                                        <button class="btn btn-outline-warning">Save for later</button>
                                    </a>
                                    <a href="/cart/delete/{{$item->rowId}}">
                                        <button type="button" class="btn btn-danger" onclick="return confirm('Are Your sure?')">
                                            Delete
                                        </button>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        <div class="d-flex justify-content-center">
                                <strong class="my-auto">
                                    Обща сума: <span id="sum">{{$total}} </span>лв.
                                </strong>
                        </div>
                        <div class="d-flex justify-content-center my-5">
                            <button class="btn btn-secondary w-75">Chekout</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
@endsection

@section('custom-script')
    <script src="{{ asset('js/change-quantity.js') }}"></script>
@endsection
