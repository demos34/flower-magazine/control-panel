<?php
namespace App\Http\Controllers\Cart;

use App\Http\Controllers\Controller;
use App\Models\Cart\Warehouse;
use App\Repositories\StorageRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use App\Repositories\WarehouseRepositoryInterface;
use Dotenv\Dotenv;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var WarehouseRepositoryInterface
     */
    private $warehouseRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository, WarehouseRepositoryInterface $warehouseRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->warehouseRepository = $warehouseRepository;
    }

    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $warehouses = $this->warehouseRepository->getAllFromWarehouse();
        $types = $this->warehouseRepository->getAllTypes();
        $categories = $this->warehouseRepository->getAllCategories();

        return view('warehouse.index')->with(
            [
                'warehouses' => $warehouses,
                'types' => $types,
                'categories' => $categories,
            ]
        );
    }

    public function show(Warehouse $warehouse)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return view('warehouse.show')->with(
            [
                'warehouse' => $warehouse,
            ]
        );
    }

    public function update(Request $request, Warehouse $warehouse)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->warehouseRepository->updateQuantity($request,$warehouse);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function view($type)
    {
        $action = 'by';
        $this->trafficRepository->getRemoteAddress($action);

        $warehouses = $this->warehouseRepository->viewBy($type);
        $types = $this->warehouseRepository->getAllTypes();
        $categories = $this->warehouseRepository->getAllCategories();

        return view('warehouse.index')->with(
            [
                'warehouses' => $warehouses,
                'types' => $types,
                'categories' => $categories,
            ]
        );
    }

    public function allOf($type, $toWhere, $id)
    {
        $action = 'allOf';
        $this->trafficRepository->getRemoteAddress($action);

        $warehouses = $this->warehouseRepository->viewAllBy($type, $toWhere, $id);
        $types = $this->warehouseRepository->getAllTypes();
        $categories = $this->warehouseRepository->getAllCategories();
        return view('warehouse.index')->with(
            [
                'warehouses' => $warehouses,
                'types' => $types,
                'categories' => $categories,
            ]
        );
    }
}
