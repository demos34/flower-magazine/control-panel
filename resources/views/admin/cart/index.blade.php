@extends('layouts.app')

@section('title')
    <title>{{ config('app.name', 'Laravel') }} | Show all carts</title>
@endsection

@section('custom-css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 my-3">
                <div class="card">
                    <div class="card-header text-center">
                        View all orders: |
                        <div class="custom-dropdown">
                            <span>View all:</span>
                            <div class="custom-dropdown-content">
                                <a href="/administrator/cart/all" style="border-bottom: 1px solid black">
                                    Orders
                                </a>
                                <br>
                                <a href="/administrator/cart/acc" style="border-bottom: 1px solid black">
                                    Accepted
                                </a>
                                <br>
                                <a href="/administrator/cart/noacc" style="border-bottom: 1px solid black">
                                    Not Accepted
                                </a>
                                <br>
                                <a href="/administrator/cart/dlv" style="border-bottom: 1px solid black">
                                    Delivered
                                </a>
                                <br>
                                <a href="/administrator/cart/nodlv" style="border-bottom: 1px solid black">
                                    Not Delivered
                                </a>
                                <br>
                                <a href="/administrator/cart/paid" style="border-bottom: 1px solid black">
                                    Paid
                                </a>
                                <br>
                                <a href="/administrator/cart/nopaid" style="border-bottom: 1px solid black">
                                    Not Paid
                                </a>
                                <br>
                                <a href="/administrator/cart/clo" style="border-bottom: 1px solid black">
                                    Closed
                                </a>
                                <br>
                                <a href="/administrator/cart/noclo" style="border-bottom: 1px solid black">
                                    Not Closed
                                </a>
                                <br>
                                <a href="/administrator/cart/del" style="border-bottom: 1px solid black">
                                    Deleted
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Names</th>
                                    <th>Address</th>
                                    <th>Accepted?</th>
                                    <th>Delivered?</th>
                                    <th>Paid?</th>
                                    <th>Closed?</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($carts as $cart)
                                <tr>
                                    <td>{{$cart->id}}</td>
                                    <td>{{$cart->names}}</td>
                                    <td>{{$cart->address}}</td>
                                    <td>@if($cart->isAccepted === 0)
                                            <strong style="color: red">No</strong>
                                            -> <a href="/administrator/acc/{{$cart->id}}"><button class="btn btn-outline-success">Accept?</button></a>
                                        @else <strong style="color: green">Yes</strong> @endif
                                    </td>
                                    <td>@if($cart->isShipped === 0)
                                            <strong style="color: red">No</strong>
                                            -> <a href="/administrator/dlv/{{$cart->id}}"><button class="btn btn-outline-success">Delivered?</button></a>
                                        @else <strong style="color: green">Yes</strong> @endif</td>
                                    <td>@if($cart->isPaid === 0)
                                            <strong style="color: red">No</strong>
                                            -> <a href="/administrator/paid/{{$cart->id}}"><button class="btn btn-outline-success">Paid?</button></a>
                                        @else <strong style="color: green">Yes</strong> @endif</td>
                                    <td>@if($cart->isClosed === 0)
                                            <strong style="color: red">No</strong>
                                            -> <a href="/administrator/clo/{{$cart->id}}"><button class="btn btn-outline-success">Close?</button></a>
                                        @else <strong style="color: green">Yes</strong> @endif</td>
                                    <td>
                                        <a href="/administrator/cart/show/{{$cart->id}}">
                                            <button type="button" class="btn btn-outline-primary">View</button>
                                        </a>
                                        @if($cart->isDeleted === 1)
                                        <a href="/administrator/res/{{$cart->id}}">
                                            <button type="button" class="btn btn-outline-dark">Restore</button>
                                        </a>
                                        @else
                                            <a href="/administrator/del/{{$cart->id}}">
                                                <button type="button" class="btn btn-outline-danger" onclick="return confirm('Are Your sure?')">Delete</button>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="d-flex justify-content-center">
                            {{$carts->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
