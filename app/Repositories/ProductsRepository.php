<?php
namespace App\Repositories;


use App\Models\Administrator\Category;
use App\Models\Administrator\Color;
use App\Models\Cart\Detail;
use App\Models\Cart\Movement;
use App\Models\Cart\Warehouse;
use App\Models\Products\Image;
use App\Models\Products\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductsRepository implements ProductsRepositoryInterface
{
    public function all()
    {
        return Product::all();
    }

    public function getAllCategories(Request $request)
    {
        $validatedTypeId = $this->validateType($request);
        return $this->getCategoriesWithTypeId($validatedTypeId['type_id']);
    }

    public function getAllCategoriesFromProductId(Product $product)
    {
        return $this->getCategoriesWithTypeId($product->category->type_id);
    }

    public function allColors()
    {
        return Color::all();
    }

    public function getAllFromCategory($categoryId)
    {
        //TODO
    }

    public function store(Request $request)
    {

        $validated = $this->validateProductsToStore($request);

        $imagePath = $this->storeImage($request);

        $product = Product::create(
            [
                'bg_name' => $validated['bg_name'],
                'eng_name' => $validated['eng_name'],
                'bg_description' => $validated['bg_description'],
                'eng_description' => $validated['eng_description'],
                'bg_slug' => $validated['bg_slug'],
                'eng_slug' => $validated['eng_slug'],
                'bg_meta_description' => $validated['bg_meta_description'],
                'eng_meta_description' => $validated['eng_meta_description'],
                'price' => $validated['price'],
                'size' => $validated['size'],
                'category_id' => $validated['category_id'],
                'color_id' => $validated['color'],
                'image' => $imagePath,
            ]
        );

        $this->createRowInWarehouse($validated, $product);


        if($request->hasFile('secondary_images')){
            $secondary_images = $this->validateSecondaryImages($request);
            foreach ($secondary_images['secondary_images'] as $secondary_image) {
                $imagePath = $this->storeSecondaryImage($secondary_image, $validated['eng_slug']);

                $image = Image::create(
                    [
                        'image' => $imagePath,
                    ]
                );

                $product->images()->attach($image->id);
            }
        }

        foreach ($validated['tags'] as $tag){
            $product->tags()->attach($tag);
        }

        foreach ($validated['keywords'] as $keyword){
            $product->keywords()->attach($keyword);
        }

        return $product;
    }

    public function update(Request $request, Product $product)
    {

        if ($request->hasFile('image') === TRUE){
            $imagePath = $this->storeImage($request);
            $this->deleteProductImage($product);
            $product->update(
                [
                    'image' => $imagePath,
                ]
            );
        }

        $this->updateProduct($request, $product);

        if($request->hasFile('secondary_images')){
            $secondary_images = $this->validateSecondaryImages($request);
            foreach ($secondary_images['secondary_images'] as $secondary_image) {
                $imagePath = $this->storeSecondaryImage($secondary_image, $product->eng_slug);

                $image = Image::create(
                    [
                        'image' => $imagePath,
                    ]
                );

                $product->images()->attach($image->id);
            }
        }
    }

    public function delete(Product $product)
    {
        foreach ($product->images as $image){
            $this->deleteSecondaryImages($image);
        }
        $this->deleteProductImage($product);
        $category = Category::find($product->category_id);
        $product->delete();

        return $category->id;
    }

    protected function updateProduct(Request $request, Product $product)
    {
        $validated = $this->validateToUpdate($request);
        $validatedSlugs = $this->validateSlugs($request, $product);

        $product->tags()->sync($validated['tags']);
        $product->keywords()->sync($validated['keywords']);

        return $product->update(
            [
                'bg_name' => $validated['bg_name'],
                'eng_name' => $validated['eng_name'],
                'bg_description' => $validated['bg_description'],
                'eng_description' => $validated['eng_description'],
                'bg_slug' => $validatedSlugs['bg_slug'],
                'eng_slug' => $validatedSlugs['eng_slug'],
                'bg_meta_description' => $validated['bg_meta_description'],
                'eng_meta_description' => $validated['eng_meta_description'],
                'price' => $validated['price'],
                'size' => $validated['size'],
                'category_id' => $validated['category_id'],
                'color_id' => $validated['color'],
            ]
        );
    }

    protected function validateSlugs(Request $request, Product $product)
    {
        if($request->bg_slug == $product->bg_slug && $request->eng_slug == $product->eng_slug){
            return $this->validateSameSlugs($request);
        } elseif ($request->bg_slug != $product->bg_slug && $request->eng_slug == $product->eng_slug) {
            return $this->validateBgSlug($request);
        } elseif ($request->bg_slug == $product->bg_slug && $request->eng_slug != $product->eng_slug){
            return $this->validateEngSlug($request);
        } else {
            return $this->validateDifferentSlugs($request);
        }
    }

    protected function validateSameSlugs(Request $request)
    {
        return $request->validate(
            [
                'bg_slug' => 'required|min:3|max:255|',
                'eng_slug' => 'required|min:3|max:255|',
            ]
        );
    }

    protected function validateDifferentSlugs(Request $request)
    {
        return $request->validate(
            [
                'bg_slug' => 'required|min:3|max:255|unique:products',
                'eng_slug' => 'required|min:3|max:255|unique:products',
            ]
        );
    }

    protected function validateEngSlug(Request $request)
    {
        return $request->validate(
            [
                'bg_slug' => 'required|min:3|max:255',
                'eng_slug' => 'required|min:3|max:255|unique:products',
            ]
        );
    }

    protected function validateBgSlug(Request $request)
    {
        return $request->validate(
            [
                'bg_slug' => 'required|min:3|max:255|unique:products',
                'eng_slug' => 'required|min:3|max:255',
            ]
        );
    }

    protected function validateToUpdate(Request $request)
    {
        return $request->validate(
            [
                'bg_name' => 'required|min:3|max:255',
                'eng_name' => 'required|min:3|max:255',
                'bg_description' => 'required|min:3|max:1000|',
                'eng_description' => 'required|min:3|max:1000|',
                'bg_meta_description' => 'required|min:3|max:1000|',
                'eng_meta_description' => 'required|min:3|max:1000|',
                'color' => 'required|exists:colors,id',
                'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'size' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'category_id' => 'required|exists:categories,id',
                'tags' => 'array|required|exists:tags,id',
                'keywords' => 'array|required|exists:keywords,id',
            ]
        );
    }

    private function validateProductsToStore(Request $request)
    {
        return $request->validate(
            [
                'bg_name' => 'required|min:3|max:255',
                'eng_name' => 'required|min:3|max:255',
                'bg_description' => 'required|min:3|max:1000|',
                'eng_description' => 'required|min:3|max:1000|',
                'bg_slug' => 'required|min:3|max:255|unique:products',
                'eng_slug' => 'required|min:3|max:255|unique:products',
                'bg_meta_description' => 'required|min:3|max:1000|',
                'eng_meta_description' => 'required|min:3|max:1000|',
                'image' => 'required|image',
                'color' => 'required|exists:colors,id',
                'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'size' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'quantity' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'metric' => 'required|string|min:1|max:20',
                'category_id' => 'required|exists:categories,id',
                'tags' => 'array|required|exists:tags,id',
                'keywords' => 'array|required|exists:keywords,id',
            ]
        );
    }

    private function storeImage(Request $request)
    {
        $validatedImage = $this->validateImage($request);

        $file = $validatedImage['image']->getClientOriginalName();
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $date = date('Y-m-d-H-i-s');
        $fileFullName = $date . '-' . $fileName . '.' . $fileExtension;
        $imagePath = '/storage/products/' . $fileFullName;
        $request->image->storeAs('products', $fileFullName, 'public');

//        $destionationPath = "C:/xampp/htdocs/leya/storage/app/public/products";
//        $request->image->move($destionationPath, $fileFullName);
        return $imagePath;
    }

    private function storeSecondaryImage($secondaryImage, $imageName)
    {
        $name = $this->getImageName($imageName);
        $file = $secondaryImage->getClientOriginalName();
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $date = date('Y-m-d-H-i-s');
        $fileFullName = $date . '-' . $name . '.' . $fileExtension;
        $imagePath = '/storage/secondary/' . $fileFullName;
        $secondaryImage->storeAs('secondary', $fileFullName, 'public');

//        $destionationPath = "C:/xampp/htdocs/leya/storage/app/public/secondary";
//        $secondaryImage->move($destionationPath, $fileFullName);
        return $imagePath;
    }

    private function validateImage(Request $request)
    {

        return $request->validate(
            [
                'image' => 'image',
            ]
        );
    }

    private function validateSecondaryImages(Request $request)
    {
        return $request->validate([
            'secondary_images.*' => 'mimes:jpg,jpeg,png,bmp|max:20000',
        ]);
    }

    protected function getImageName($imageName)
    {
        $length = 20;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString . '-' . $imageName;
    }

    protected function deleteProductImage(Product $product)
    {
        $originalImage = $product->image;
        $array = explode('/', $originalImage);
        $fileName = $array['3'];
//        $destionationPath = "C:/xampp/htdocs/leya/storage/app/public/products/" . $fileName;
//        unlink($destionationPath);
        $path = '/public/' . $array['2'] . '/' . $fileName;
        Storage::delete($path);
    }

    protected function deleteSecondaryImages(Image $image)
    {
        $originalImage = $image->image;
        $array = explode('/', $originalImage);
        $fileName = $array['3'];
//        $destionationPath = "C:/xampp/htdocs/leya/storage/app/public/secondary/" . $fileName;
//        unlink($destionationPath);
        $path = '/public/' . $array['2'] . '/' . $fileName;
        Storage::delete($path);
        return $image->delete();
    }

    public function deleteSecondaryImagesFromController(Image $image)
    {
        $productId = implode(',', $image->products()->get()->pluck('id')->toArray());
        $this->deleteSecondaryImages($image);
        $image->delete();
        return Product::find($productId)->firstOrFail();
    }

    protected function validateType(Request $request)
    {
        return $request->validate(
            [
                'type_id' => 'required|exists:types,id',
            ]
        );
    }

    protected function getCategoriesWithTypeId($type_id)
    {
        return Category::where('type_id', $type_id)->get();
    }

    protected function createRowInWarehouse($validated, Product $product)
    {
        $category = Category::findOrFail($product->category_id);

        $warehouse = Warehouse::create(
            [
                'category_id' => $category->id,
                'product_id' => $product->id,
                'type_id' => $category->type_id,
                'quantity' => $validated['quantity'],
                'metric' => $validated['metric'],
                'is_deleted' => false,
            ]
        );

        $movement = Movement::create(
            [
                'warehouse_id' => $warehouse->id,
                'to_where' => 'create',
                'with_how_many' => $warehouse->quantity,
                'is_buying' => false,
                'is_warehouse_deleted' => false,
            ]
        );

        Detail::create(
            [
                'movement_id' => $movement->id,
                'old' => 0.00,
                'by_how_much' => $warehouse->quantity,
                'new' => (float)($warehouse->quantity),
                'to_where' => 'create',
            ]
        );
    }
}
