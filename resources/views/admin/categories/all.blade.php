@extends('layouts.app')


@section('title')
    <title>{{ config('app.name', '') }} | Products of {{$category->eng_name}} | Продукти от {{$category->bg_name}}</title>
@endsection

@section('custom-css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
@endsection


@section('content')
    @include('cookieConsent::index')
    <div class="container">
        <div class="justify-content-between">
            <div class="py-2">
                <a href="/category/{{$category->id}}">
                    <button class="btn btn-outline-primary">Back</button>
                </a>
                <a href="/products/create/{{$category->type_id}}" class="float-right">
                    <button class="btn btn-outline-success">Add product</button>
                </a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div>
                    <div class="text-center">
                        Всички продукти от категория <strong>{{$category->bg_name}} / {{$category->eng_name}}</strong>:
                    </div>
                    <div class="text-center my-5">
                        <div class="custom-dropdown">
                            <span>Sort items by:</span>

                            <div class="custom-dropdown-content">
                                @foreach($sort as $sortBy)
                                    <a href="/category/sort/{{$sortBy->id}}/{{$category->id}}">
                                        {{$sortBy->bg_sort}}
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="well p-1 m-1" style="background-color: antiquewhite; height: 10em">
                                <label >
                                        Цвят
                                </label>
                                <br>
                                <span>
                                    <a href="/category/color/1/{{$category->id}}">
                                        Виж всички цветове:
                                    </a>
                                </span>
                                <hr>
                                <div class="row">
                                    @foreach($colors as $color)
                                        @if($color->id > 1)
                                            <div class="col-sm-4">
                                            <span>
                                                <a href="/category/color/{{$color->id}}/{{$category->id}}">
                                                <img src="{{$color->image}}" alt="" style="width: 1.1em; height: 1.1em"
                                                     class="rounded-circle float-right">
                                                </a>
                                            </span>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 float-right">
                            <div class="row">

                                @foreach($products as $product)
                                    <div class="col-md-3 m-2">
                                        <div>
                                            <a href="/products/{{$product->id}}">
                                                <img src="{{$product->image}}" alt="" style="width: 15em; height: 15em">
                                            </a>
                                        </div>
                                        <div class="text-center">
                                            {{$product->price}} лв
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
