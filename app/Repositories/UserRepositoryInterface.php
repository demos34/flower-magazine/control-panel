<?php

namespace App\Repositories;

use App\User;
use Illuminate\Http\Request;

interface UserRepositoryInterface
{
    public function all();

    public function allRoles();

    public function update(Request $request, User $user);

    public function delete(User $user);

    public function changePassword(Request $request, User $user);

    public function register(Request $request);
}
