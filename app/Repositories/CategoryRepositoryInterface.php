<?php

namespace App\Repositories;

use App\Models\Administrator\Category;
use App\Models\Administrator\SortItem;
use App\Models\Types\Type;
use Illuminate\Http\Request;

interface CategoryRepositoryInterface
{
    public function getAll();

    public function getAllSortBy();

    public function getAllProductsSortedBy(Category $category);

    public function getAllColors();

    public function getAllTags();

    public function getAllKeywords();

    public function store(Request $request, Type $type);

    public function update(Request $request, Category $category);

    public function delete(Category $category);

    public function animStore(Request $request, Category $category);

    public function animUpdate(Request $request, Category $category);
}
