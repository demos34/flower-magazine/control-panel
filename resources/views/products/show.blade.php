@extends('layouts.app')

@section('title')
    <title>{{ config('app.name', 'Laravel') }} | Show {{$product->eng_name}}</title>
@endsection

@section('custom-css')
    @livewireStyles
@endsection

@section('content')
    <div class="container">

        <div class="row" style="padding-top: 5em; padding-left: 3em; padding-right: 3em">
            <div class="col-lg-4 my-1 p-1" style="background-color: whitesmoke;">
                <div class="well my-2 justify-content-center" id="image">
                    <img src="{{$product->image}}" alt="" style="width: 16em" id="showed_image">
                </div>
                <div class="justify-content-center">
                    <span class="secondary-image">
                        <img src="{{$product->image}}" style="height: 4em; width: 4em" alt="" onclick="func(this.src)">
                    </span>
                    @foreach($product->images as $image)
                        <span class="secondary-image">
                            <img src="{{$image->image}}" style="height: 4em; width: 4em" alt="" onclick="func(this.src)">
                        </span>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-8 my-1 p-1" style="background-color: lightyellow">
                <div class="d-flex justify-content-center">
                    <div class="text-center" style="padding-top: 5em; padding-bottom: 3em">
                        <h1>
                            {{$product->bg_name}} / {{$product->eng_name}}
                        </h1>
                        <p>
                        @if($product->warehouse->quantity == 0)
                            <strong style="color: red">В момента този продукт не е наличен. Моля, свържете се с нас, за да ви го набавим или просто изберете друг продукт</strong>
                        @else
                            <strong style="color: green">В наличност</strong>
                        @endif
                        </p>
                    </div>
                </div>
                <form action="/cart/{{$product->id}}" method="POST">
                    @csrf
                <div class="d-flex justify-content-center">
                    <div class="well pr-5">
                        <p>
                        <span>
                            размер: {{$product->size}} cm
                        </span>
                        </p>
                        <p>
                        <span>
                            цвят: {{implode(',', $product->color()->get()->pluck('bg_color')->toArray())}} / {{implode(',', $product->color()->get()->pluck('eng_color')->toArray())}}
                        </span>
                        </p>
                        <p>
                        <span>
                            <span onclick="show()" class="description" style="text-decoration: none; color: black">Описание</span>
                        </span>
                        </p>
                    </div>
                    <div class="pl-5">
                        <p>
                        <h2>
                                <span class="pr-5">
                                        {{$product->price}} лв.
                                </span>
                        </h2>
                        </p>
                        <p>
                            <span>
                                @livewire('quantity')
                            </span>
                        </p>
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    @if($product->warehouse->quantity == 0)
                        <strong style="color: red">Изчерпан</strong>
                    @else
                        <button class="btn w-50 font-weight-bolder"
                                style="background-color: pink; text-transform: uppercase">Добави в количката
                        </button>
                    @endif
                </div>
                </form>
                <div class="d-flex justify-content-between m-5">
                    <a href="/products/{{$product->id}}/edit">
                        <button class="btn btn-warning">
                            Edit
                        </button>
                    </a>
                    <form action="/products/{{$product->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" onclick="return confirm('Are you sure?')">
                            Delete
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <div style="display:none; background-color: wheat" class="col-md-10 text-center float-left" id="show">
            <p>
                {{$product->bg_description}}
            </p>
            <p>
                <span onclick="hide()" class="description" style="text-decoration: none; color: black">По-малко</span>
            </p>
        </div>
    </div>
@endsection

@section('custom-script')
    @livewireScripts
    <script src="{{ asset('js/change-src.js') }}"></script>
    <script src="{{ asset('js/show-hide-description.js') }}"></script>
@endsection

