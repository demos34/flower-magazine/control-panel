<?php

namespace App\Http\Controllers\Seo;

use App\Http\Controllers\Controller;
use App\Models\Tags\IndexDescription;
use App\Models\Tags\IndexTag;
use App\Models\Tags\Tag;
use App\Repositories\TagsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class TagsController extends Controller
{

    private $trafficRepository;
    /**
     * @var TagsRepositoryInterface
     */
    private $tagsRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                TagsRepositoryInterface $tagsRepository)
    {
        $this->middleware('auth');
        $this->trafficRepository = $trafficRepository;
        $this->tagsRepository = $tagsRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $tags = $this->tagsRepository->getAllTags();
        $indexTags = $this->tagsRepository->getAllIndexTags();
        $description = $this->tagsRepository->getIndexDescription();
        $keywords = $this->tagsRepository->getAllKeywords();

        return view('seo.tags.index')->with(
            [
                'tags' => $tags,
                'indexTags' => $indexTags,
                'description' => $description,
                'keywords' => $keywords,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeTags(Request $request)
    {
        $action = 'storeTags';
        $this->trafficRepository->getRemoteAddress($action);

        $this->tagsRepository->storeTag($request);
        return $this->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeIndexTags(Request $request)
    {
        $action = 'storeIndexTags';
        $this->trafficRepository->getRemoteAddress($action);

        $this->tagsRepository->storeIndexTag($request);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $tag
     *
     */
    public function show(Tag $tag)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return view('seo.tags.show')->with('tag', $tag);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $indexTag
     * @return \Illuminate\Http\Response
     */
    public function showIndexTag(IndexTag $indexTag)
    {
        $action = 'showIndexTag';
        $this->trafficRepository->getRemoteAddress($action);

        return view('seo.index.show')->with('tag', $indexTag);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $indexDescription
     * @return \Illuminate\Http\Response
     */
    public function showIndexDescription(IndexDescription $indexDescription)
    {
        $action = 'showIndexDescription';
        $this->trafficRepository->getRemoteAddress($action);

        return view('seo.description.show')->with('description', $indexDescription);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $action = 'storeIndexTags';
        $this->trafficRepository->getRemoteAddress($action);

        $update = $this->tagsRepository->updateTag($request, $tag);

        return $this->show($tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $indexTag
     * @return \Illuminate\Http\Response
     */
    public function updateIndexTag(Request $request, IndexTag $indexTag)
    {
        $action = 'storeIndexTags';
        $this->trafficRepository->getRemoteAddress($action);

        $update = $this->tagsRepository->updateIndexTag($request, $indexTag);
        return $this->showIndexTag($indexTag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $indexDescription
     * @return \Illuminate\Http\Response
     */
    public function updateIndexDescription(Request $request, IndexDescription $indexDescription)
    {
        $action = 'storeIndexTags';
        $this->trafficRepository->getRemoteAddress($action);

        $update = $this->tagsRepository->updateDescription($request, $indexDescription);
        return $this->showIndexDescription($indexDescription);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $action = 'storeIndexTags';
        $this->trafficRepository->getRemoteAddress($action);

        $this->tagsRepository->deleteTag($tag);
        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $indexTag
     * @return \Illuminate\Http\Response
     */
    public function destroyIndexTag(IndexTag $indexTag)
    {
        $action = 'storeIndexTags';
        $this->trafficRepository->getRemoteAddress($action);

        $this->tagsRepository->deleteIndexTag($indexTag);
        return $this->index();
    }
}
