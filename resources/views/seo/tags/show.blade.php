@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div>
                    <a href="/seo/">
                        <button class="btn btn-outline-dark">Back</button>
                    </a>
                    <form method="post" action="/seo/{{$tag->id}}" class="float-right">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-outline-danger">Delete</button>
                    </form>
                </div>
                <div class="card my-3 mx-auto">
                    <div class="card-header">
                        <h4 align="center">
                            Мета-таг за продукти <strong style="color: blue">{{$tag->bg_tag}}
                                / {{$tag->eng_tag}}</strong>, използван в следните продукти:
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="card-group">
                            <div class="mx-5">#</div>
                            <div class="mx-5">Product type</div>
                            <div class="mx-auto">Product</div>
                        </div>
                        <hr>
                        <div class="card-group">
                            <div class="mx-5">ID</div>
                            <div class="mx-4">
                                <a href="#">Опаковки / Цветя</a>
                            </div>
                            <div class="mx-auto">
                                <a href="#">Продукт(опаковка/цвете)</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-3">
                <div class="well">
                    <h4 align="center">
                        <label>
                            Добави нов мета-таг
                        </label>
                    </h4>
                    <form action="/seo/{{$tag->id}}/" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="card">
                            <div class="card-header">
                                <label for="bg_tag">
                                    Bg Tag:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="bg_tag"
                                           type="text"
                                           class="form-control @error('bg_tag') is-invalid @enderror"
                                           name="bg_tag"
                                           value=@if(old('bg_tag'))"{{ old('bg_tag') }}"@else"{{$tag->bg_tag}}"@endif
                                    required autocomplete="bg_tag" autofocus>
                                    @error('bg_tag')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                    @enderror
                                </h5>
                            </div>
                            <div class="card-header">
                                <label for="eng_tag">
                                    Eng Tag:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="eng_tag"
                                           type="text"
                                           class="form-control @error('eng_tag') is-invalid @enderror"
                                           name="eng_tag"
                                           value=@if(old('eng_tag'))"{{ old('eng_tag') }}"@else"{{$tag->eng_tag}}"@endif
                                           required autocomplete="eng_tag" autofocus>
                                    @error('eng_tag')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
