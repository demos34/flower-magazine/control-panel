@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="py-2">
                    <a href="/users/">
                        <button class="btn btn-outline-primary">Back</button>
                    </a>
                </div>
                <div class="card">
                    <div class="card-header" align="center">
                        Потребители в контролния панел:
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>#</td>
                                <td>{{$user->id}}</td>
                            </tr>
                            <tr>
                                <td>Username:</td>
                                <td>{{$user->username}}</td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td>{{$user->name}}</td>
                            </tr>
                            <tr>
                                <td>E-mail</td>
                                <td>{{$user->email}}</td>
                            </tr>
                            <tr>
                                <td>Role</td>
                                <td>{{implode(',', $user->roles()->get()->pluck('role')->toArray())}}</td>
                            </tr>
                            <tr>
                                <td>Delete</td>
                                <td>
                                    <form action="/users/{{$user->id}}" method="POST" class="float-left">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" onclick="return confirm('Are you sure?')"
                                                class="btn btn-outline-danger">Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="py-2" align="center">
                    <strong>
                        Edit user:
                    </strong>
                </div>
                <div class="well mx-auto">
                    <form action="/users/{{$user->id}}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="card">
                            <div class="card-header">
                                <label for="username">
                                    Username:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="username"
                                           type="text"
                                           class="form-control @error('username') is-invalid @enderror"
                                           name="username"
                                           value=@if(old('username'))"{{ old('username') }}"@else"{{$user->username}}"@endif
                                    required autocomplete="username" autofocus>
                                    @error('username')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <div class="card-header">
                                <label for="name">
                                    Name:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="name"
                                           type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name"
                                           value=@if(old('name'))"{{ old('name') }}"@else"{{$user->name}}"@endif
                                    required autocomplete="name" autofocus>
                                    @error('name')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <div class="card-header">
                                <label for="email">
                                    E-mail:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="email"
                                           type="text"
                                           class="form-control @error('email') is-invalid @enderror"
                                           name="email"
                                           value=@if(old('email'))"{{ old('email') }}"@else"{{$user->email}}"@endif
                                    required autocomplete="email" autofocus>
                                    @error('email')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <div class="card-header">
                                <label for="role">
                                    Roles:
                                </label>
                            </div>
                            <div class="card-body">
                                @foreach($roles as $role)
                                    <div class="mx-5">
                                        <input id="role"
                                               type="radio"
                                               name="role"
                                               value="{{$role->id}}"
                                               required autocomplete="role" autofocus>
                                        {{$role->role}}
                                    </div>
                                @endforeach
                                @error('role')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Edit user
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <div class="py-2" align="center">
                    <strong>
                        Change user's password:
                    </strong>
                </div>
                <div class="well mx-auto">
                    <form action="/users/password/{{$user->id}}" method="POST" class="card">
                        @csrf
                        @method('PATCH')
                        <div class="card-body">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                            <div>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="card-body">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="well mx-auto my-5">
                            <button type="submit" class="btn btn-primary">Change</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
