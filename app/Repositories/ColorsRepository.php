<?php
namespace App\Repositories;


use App\Models\Administrator\Color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ColorsRepository implements ColorsRepositoryInterface
{
    public function getAll()
    {
        return Color::all();
    }

    public function store(Request $request)
    {
        $validated = $this->validateColorToStore($request);

        $imagePath = $this->storeImage($request);

        return Color::create(
            [
                'bg_color' => $validated['bg_color'],
                'eng_color' => $validated['eng_color'],
                'image' => $imagePath,
            ]
        );

    }

    public function update(Request $request, Color $color)
    {
        $validated = $this->validateUpdate($request, $color);

        if ($request->hasFile('image') === FALSE){
            return $color->update(
                [
                    'bg_color' => $validated['bg_color'],
                    'eng_color' => $validated['eng_color'],
                ]
            );
        } else {
            $imagePath = $this->storeImage($request);

            $originalImage = $color->image;
            $array = explode('/', $originalImage);
            $fileName = $array['3'];
//            $destionationPath = "C:/xampp/htdocs/leya/storage/app/public/categories/" . $fileName;
//            unlink($destionationPath);
            $path = '/public/' . $array['2'] . '/' . $fileName;
            Storage::delete($path);


            return $color->update(
                [
                    'bg_color' => $validated['bg_color'],
                    'eng_color' => $validated['eng_color'],
                    'image' => $imagePath,
                ]
            );
        }
    }

    public function delete(Color $color)
    {
        $originalImage = $color->image;
        $array = explode('/', $originalImage);
        $path = '/public/' . $array['2'] . '/' .$array['3'];
        Storage::delete($path);

        return $color->delete();
    }

    private function validateUpdate(Request $request, Color $color)
    {
        if($request->bg_color == $color->bg_color && $request->eng_color == $color->eng_color){
            $validated = $this->validateSameColors($request);
        } elseif ($request->bg_color == $color->bg_color && $request->eng_color != $color->eng_color){
            $validated = $this->validateEngColor($request);
        } elseif ($request->bg_color != $color->bg_color && $request->eng_color == $color->eng_color){
            $validated = $this->validateBgColor($request);
        } else {
            $validated = $this->validateColorToUpdateWithoutImage($request);
        }
        return $validated;
    }

    private function validateColorToStore(Request $request)
    {
        return $request->validate(
            [
                'bg_color' => 'required|min:3|max:100|unique:colors',
                'eng_color' => 'required|min:3|max:100|unique:colors',
                'image' => 'required|image',
            ]
        );
    }

    private function storeImage(Request $request)
    {
        $validatedImage = $this->validateImage($request);

        $file = $validatedImage['image']->getClientOriginalName();
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $date = date('Y-m-d-H-i-s');
        $fileFullName = $date . '-' . $fileName . '.' . $fileExtension;
        $imagePath = '/storage/colors/' . $fileFullName;
        $request->image->storeAs('colors', $fileFullName, 'public');

//        $destionationPath = "C:/xampp/htdocs/leya/storage/app/public/colors";
//        $request->image->move($destionationPath, $fileFullName);

        return $imagePath;
    }

    private function validateImage(Request $request)
    {
        return $request->validate(
            [
                'image' => 'image',
            ]
        );
    }

    private function validateBgColor(Request $request)
    {
        return $request->validate(
            [
                'bg_color' => 'required|min:3|max:100|unique:colors',
                'eng_color' => 'required',
            ]
        );
    }

    private function validateSameColors(Request $request)
    {
        return $request->validate(
            [
                'bg_color' => 'required',
                'eng_color' => 'required',
            ]
        );
    }

    private function validateEngColor(Request $request)
    {
        return $request->validate(
            [
                'bg_color' => 'required',
                'eng_color' => 'required|min:3|max:100|unique:colors',
            ]
        );
    }

    private function validateColorToUpdateWithoutImage(Request $request)
    {
        return $request->validate(
            [
                'bg_color' => 'required|min:3|max:100|unique:colors',
                'eng_color' => 'required|min:3|max:100|unique:colors',
            ]
        );
    }
}
