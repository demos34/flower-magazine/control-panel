<?php

namespace App\Providers;

use App\Models\Administrator\Category;
use App\Models\Administrator\Message;
use App\Models\Types\Type;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('partials.navbar', function ($view) {
            if(!empty(Auth::user()->id))
            {
                $userId = Auth::user()->id;
            }
            else {
                $userId = 4;
            }

            $view->with
            ([
                'categories' => Category::all(),
                'types' => Type::all(),
                'total' => Cart::subtotal(),
                'messages' => Message::where('user_id', $userId),
                'newMessages' => Message::where(
                    [
                        'user_id' => $userId,
                        'isRead' => false,
                        'isDeleted' => false,
                    ]
                )->get(),
            ]);
        });
    }
}
