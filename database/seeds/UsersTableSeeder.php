<?php

use App\Models\Administrator\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('role_user')->truncate();
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $administratorRole = Role::where('role', 'Administrator')->first();
        $developerRole = Role::where('role', 'Developer')->first();
        $genericUserRole = Role::where('role', 'Generic User')->first();

        $genericUser = User::create(
            [
                'username' => 'Generic',
                'name' => 'Generic User',
                'email' => 'generic_user@admin.com',
                'password' => Hash::make('useruser')

            ]
        );

        $testUser = User::create(
            [
                'username' => 'Test',
                'name' => 'Test User',
                'email' => 'test_user@admin.com',
                'password' => Hash::make('useruser')

            ]
        );

        $administrator = User::create(
            [
                'username' => 'Administrator',
                'name' => 'Administrator',
                'email' => 'administrator@admin.com',
                'password' => Hash::make('123123123')

            ]
        );

        $developer = User::create(
            [
                'username' => 'Developer',
                'name' => 'Developer',
                'email' => 'ins0mniac1van@gmail.com',
                'password' => Hash::make('mer89621')

            ]
        );

        $genericUser->roles()->attach($genericUserRole);
        $testUser->roles()->attach($genericUserRole);
        $administrator->roles()->attach($administratorRole);
        $developer->roles()->attach($developerRole);
    }
}
