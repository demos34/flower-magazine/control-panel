@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div>
                    <a href="/administrator/">
                        <button class="btn btn-outline-dark">Back</button>
                    </a>
                </div>
                <div class="card my-3 mx-auto">
                    <div class="card-header">
                        <h4 class="text-center">
                            <strong>Sort items by:</strong>
                        </h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Sort by</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($sortBy->count() > 0)
                                @foreach($sortBy as $sort)
                                    <tr>
                                        <td>
                                            {{$sort->id}}
                                        </td>
                                        <td>
                                            <strong>
                                                {{$sort->bg_sort}} / {{$sort->eng_sort}}
                                            </strong>
                                        </td>
                                        <td>
                                            <a href="/sort/{{$sort->id}}">
                                                <button class="btn btn-outline-success float-left">View</button>
                                            </a>
                                            <form action="/sort/{{$sort->id}}" method="POST" class="float-right">
                                                @csrf
                                                @method('DELETE')
                                                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-outline-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                            <tr>
                                No colors yet!
                            </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-3">
                <div class="well">
                    <h4 class="text-center">
                        <label>
                            Добави тип сортиране
                        </label>
                    </h4>
                    <form action="/sort" method="POST">
                        @csrf
                        <div class="card">
                            <div class="card-header">
                                <label for="bg_sort">
                                    BG Sort by:
                                </label>
                            </div>
                            <div class="card-body">
                                    <input id="bg_sort"
                                           type="text"
                                           class="form-control @error('bg_sort') is-invalid @enderror"
                                           name="bg_sort"
                                           value="{{ old('bg_sort') }}"
                                    required autocomplete="bg_sort" autofocus>
                                    @error('bg_sort')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <div class="card-header">
                                <label for="eng_sort">
                                    ENG sort by:
                                </label>
                            </div>
                            <div class="card-body">
                                    <input id="eng_sort"
                                           type="text"
                                           class="form-control @error('eng_sort') is-invalid @enderror"
                                           name="eng_sort"
                                           value="{{ old('eng_sort') }}"
                                    required autocomplete="eng_sort" autofocus>
                                    @error('eng_sort')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <div class="card-header">
                                <label for="column">
                                    Wich column will be sorted:
                                </label>
                            </div>
                            <div class="card-body">
                                    <input id="column"
                                           type="text"
                                           class="form-control @error('column') is-invalid @enderror"
                                           name="column"
                                           value="{{ old('column') }}"
                                    required autocomplete="column" autofocus>
                                    @error('column')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <div class="card-header">
                                <label for="to_where">
                                    Sort to where (Increment or Decrement):
                                </label>
                            </div>
                            <div class="card-body">
                                <select class="form-control" name="to_where" id="to_where">
                                        <option value="asc">
                                            Increment
                                        </option>
                                        <option value="desc">
                                            Decrement
                                        </option>
                                </select>
                                    @error('to_where')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                        </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

