@extends('layouts.app')

@section('custom-css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header text-center" >
                        Административен контролен панел
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-header text-center" ><a href="/users/">Потребители в контролния панел</a></div>
                </div>
                <br>
                <div class="card">
                    <div class="card-header text-center" ><a href="/colors">Управеление на цветовете в продуктите</a></div>
                </div>
                <br>
                <div class="card">
                    <div class="card-header text-center" ><a href="/sort">Управление на начина на сортиране</a></div>
                </div>
                <br>
                <div class="card">
                    <div class="card-header text-center" ><a href="/types">Управление на типовете категории</a></div>
                </div>
                <br>
            </div>
        </div>
    </div>
@endsection
