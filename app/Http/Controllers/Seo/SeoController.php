<?php

namespace App\Http\Controllers\Seo;

use App\Http\Controllers\Controller;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class SeoController extends Controller
{

    private $trafficRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository)
    {
        $this->trafficRepository = $trafficRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);
        return 'seo index';
    }
}
