@extends('layouts.app')

@section('title')
    <title>{{ config('app.name', 'Laravel') }} | Summary:</title>
@endsection

@section('content')
    <div class="container">
        <div class="d-flex justify-content-center">
            <div class="btn justify-content-center" style="background-color: #e67cb9">
                <h2>ВАШАТА КОЛИЧКА</h2>
            </div>
        </div>
        <div class="d-flex justify-content-center my-2">
            <div class="d-flex justify-content-center">
                <strong>
                    Обща сума: {{$total}} лв.
                </strong>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <div class="row justify-content-center">
                <form class="form-group" action="/purchase" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="col-md-10 my-3">
                        <label for="names" class="form-check-label">
                            Имена:
                        </label>
                        <input
                            id="names"
                            name="names"
                            class="form-control @error('names') is-invalid @enderror" type="text" style="width: 25em"
                            value="{{ old('names') }}">
                        @error('names')
                        <br>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-10 my-3">
                        <label for="phone" class="form-check-label">
                            Телефон:
                        </label>
                        <input
                            id="phone"
                            name="phone"
                            class="form-control @error('phone') is-invalid @enderror" type="text" style="width: 25em"
                            value="{{ old('phone') }}"
                            required autocomplete="phone" autofocus>
                        @error('phone')
                        <br>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-10 my-3">
                        <label for="address" class="form-check-label">
                            Адрес:
                        </label>
                        <input
                            id="address"
                            name="address"
                            class="form-control @error('address') is-invalid @enderror" type="text" style="width: 25em"
                            value="{{ old('address') }}"
                            required autocomplete="address" autofocus>
                        @error('address')
                        <br>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-10 my-3">
                        <label for="email" class="form-check-label">
                            Имейл:
                        </label>
                        <input
                            id="email"
                            name="email"
                            class="form-control @error('email') is-invalid @enderror" type="email" style="width: 25em"
                            value="{{ old('email') }}"
                            required autocomplete="email" autofocus>
                        @error('email')
                        <br>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-10 my-3">
                        <label for="additional_info" class="form-check-label">
                            Допълнителна информация:
                        </label>
                        <textarea
                            id="additional_info"
                            name="additional_info"
                            placeholder="Допълнителна информация - като за кога Ви трябват тези продукти"
                            style="height: 10em; width: 25em"
                            class="form-control @error('additional_info') is-invalid @enderror" type="additional_info" style="width: 25em"
                            required autocomplete="email" autofocus>{{ old('additional_info') }}</textarea>
                        @error('additional_info')
                        <br>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="d-flex justify-content-center">
                        <button class="btn btn-success">
                            Край
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
