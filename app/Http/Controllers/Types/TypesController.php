<?php

namespace App\Http\Controllers\Types;

use App\Http\Controllers\Controller;
use App\Models\Types\Type;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use App\Repositories\TypesRepositoryInterface;
use Illuminate\Http\Request;

class TypesController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var TypesRepositoryInterface
     */
    private $typesRepository;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                TypesRepositoryInterface $typesRepository,
                                CategoryRepositoryInterface $categoryRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->typesRepository = $typesRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $types = $this->typesRepository->getAll();
        $tags = $this->categoryRepository->getAllTags();
        $keywords = $this->categoryRepository->getAllKeywords();

        return view('types.index')->with(
            [
                'types' => $types,
                'tags' => $tags,
                'keywords' => $keywords,
            ]
        );
    }

    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->typesRepository->storeType($request);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function show(Type $type)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        $tags = $this->categoryRepository->getAllTags();
        $keywords = $this->categoryRepository->getAllKeywords();

        return view('types.show')->with(
            [
                'type' => $type,
                'tags' => $tags,
                'keywords' => $keywords,
            ]
        );
    }

    public function update(Request $request, Type $type)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->typesRepository->updateType($request, $type);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function  chooseType()
    {
        $action = 'chooseType';
        $this->trafficRepository->getRemoteAddress($action);

        $types = $this->typesRepository->getAll();

        return view('types.create')->with('types', $types);
    }

    public function destroy(Type $type)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->typesRepository->deleteType($type);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function anim(Type $type)
    {
        $action = 'anim';
        $this->trafficRepository->getRemoteAddress($action);

        return view('types.anim')->with(
            [
                'type' => $type,
            ]
        );
    }

    public function animStore(Request $request, Type $type)
    {
        $action = 'animStore';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->typesRepository->animStore($request, $type);
        return redirect('/types/'.$type->id)->with($msg['session'], $msg['message']);
    }

    public function animEdit(Type $type)
    {
        $action = 'animStore';
        $this->trafficRepository->getRemoteAddress($action);

        return view('types.anim-show')->with(
            [
                'type' => $type,
            ]
        );
    }

    public function animUpdate(Request $request, Type $type)
    {
        $action = 'animStore';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->typesRepository->animUpdate($request, $type);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

}
