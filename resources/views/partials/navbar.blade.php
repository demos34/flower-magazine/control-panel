<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
            <a class="navbar-brand" href="/">{{ config('app.name', 'Leya') }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                    aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            @can('admin-dev')
            <ul class="navbar-nav mr-auto">
{{--                <li class="nav-item active">--}}
{{--                    <a class="nav-link" href="/">За нас--}}
{{--                        <span class="sr-only">(current)</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Продукти <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
                            @foreach($types as $type)
                            <a class="dropdown-item" href="/category/index/{{$type->id}}">
                                {{$type->bg_name}} / {{$type->eng_name}}
                            </a>
                            @endforeach
                            <a class="dropdown-item" href="/types">
                                Виж всички типове категории
                            </a>
                            <a class="dropdown-item" href="/types/create/product">
                                Добави нов продукт
                            </a>
                        </div>
                    </li>
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link" href="/contacts">Контакти</a>--}}
{{--                </li>--}}
                @endcan
            </ul>
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->

                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a href="/administrator/cart" class="nav-link">All orders</a>
                    </li>
                    <li class="nav-item">
                        <a href="/messages" class="nav-link">Message @if($newMessages->count() > 0) <small
                                style="background-color: green; padding: 1px">{{$newMessages->count()}}
                                new</small> @endif</a>
                    </li>
                    <li class="nav-item">
                        <a href="/cart" class="nav-link">Cart <small>{{$total}} лв.</small></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="text-transform: uppercase">
                            {{App::getLocale()}} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/lang/bg">
                                BG
                            </a>
                            <a class="dropdown-item" href="/lang/en">
                                EN
                            </a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @can('admin-dev')
                                <a class="dropdown-item" href="/warehouse">Warehouse</a>
                                <a class="dropdown-item" href="/administrator/">Control Panel</a>
                                <a class="dropdown-item" href="/seo/" style="color: green">SEO</a>
                            @endcan
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
