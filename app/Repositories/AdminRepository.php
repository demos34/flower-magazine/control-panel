<?php
namespace App\Repositories;


use App\Models\Administrator\Message;
use App\Models\Administrator\SortItem;
use App\Models\Cart\Buying;
use App\Models\Cart\Cart;
use App\Models\Cart\Detail;
use App\Models\Cart\Movement;
use App\Models\Products\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminRepository implements AdminRepositoryInterface
{
    public function viewAllSortBy()
    {
        return SortItem::all();
    }

    public function storeSortBy(Request $request)
    {
        return $this->storeValidatedSortBy($request);
    }

    public function updateSortBy(Request $request, SortItem $sortItem)
    {
        return $this->updateValidatedSortBy($request, $sortItem);
    }

    public function deleteSortBy(SortItem $sortItem)
    {
        return $sortItem->delete();
    }

    public function getAllUnreadMessages()
    {
        return Message::where(
            [
                'user_id' => Auth::user()->id,
                'isDeleted' => false,
            ]
        )->orderBy('created_at', 'DESC')->get();
    }

    public function setMessagesAsRead($messages)
    {
        foreach ($messages as $message){
            $message->update(
                [
                    'isRead' => true,
                ]
            );
        }
    }

    public function deleteMessage(Message $message)
    {
        return $message->delete();
    }

    public function getAllAdminCart()
    {
        return Cart::where('isDeleted', false)->orderBy('isAccepted')->orderBy('created_at', 'DESC')->paginate(10);
    }

    public function getAllAdminCartSorted($status)
    {
        if($status == 'all'){
            return $this->getAllAdminCart();
        }
        $data = $this->getMethodAndBool($status);
        return Cart::where($data['method'], $data['bool'])->orderBy('created_at', 'desc')->paginate(10);

    }

    public function changeStatus($status, Cart $cart)
    {
        $data = $this->getMethodAndBool($status, $cart);
        if($cart->update(
            [
                $data['method'] => $data['bool'],
            ]
        )){
            $msg = [
                'session' => 'success',
                'message' => 'The field ' . $data['method'] . ' is successfully updated',
            ];
        } else {
            $msg = [
                'session' => 'alert',
                'message' => 'The field ' . $data['method'] . ' is not updated',
            ];
        }
        return $msg;
    }

    public function deleteCart(Cart $cart)
    {
        if($cart->update(
            [
                'isDeleted' => true,
            ]
        )){
            $msg = [
                'session' => 'success',
                'message' => 'This cart is deleted!',
            ];
        } else {
            $msg = [
                'session' => 'alert',
                'message' => 'Ooops, something is wrong with this cart!',
            ];
        }
        return $msg;
    }

    protected function storeValidatedSortBy(Request $request)
    {
        $validated = $this->validateSortBy($request);
        if(SortItem::create(
            [
                'bg_sort' => $validated['bg_sort'],
                'eng_sort' => $validated['eng_sort'],
                'column' => $validated['column'],
                'to_where' => $validated['to_where'],
            ]
        )){
            $msg = [
                'session' => 'success',
                'message' => 'The record is successfully updated!',
            ];

        } else {
            $msg = [
                'session' => 'alert',
                'message' => 'Ooops! Something get wrong!',
            ];
        }

        return $msg;
    }

    protected function updateValidatedSortBy(Request $request, SortItem $sortItem)
    {
        $validated = $this->validateSort($request, $sortItem);
        if($sortItem->update(
            [
                'bg_sort' => $validated['bg_sort'],
                'eng_sort' => $validated['eng_sort'],
                'column' => $validated['column'],
                'to_where' => $validated['to_where'],
            ]
        )){
            $msg = [
                'session' => 'success',
                'message' => 'The record is successfully updated!',
            ];

        } else {
            $msg = [
                'session' => 'alert',
                'message' => 'Ooops! Something get wrong!',
            ];
        }

        return $msg;
    }

    protected function validateSort(Request $request, SortItem $sortItem)
    {
        if($request->bg_sort == $sortItem->bg_sort && $request->eng_sort == $sortItem->eng_sort){
            $validated = $this->validateSameSorts($request);
        } elseif($request->bg_sort != $sortItem->bg_sort && $request->eng_sort == $sortItem->eng_sort) {
            $validated = $this->validateBgSort($request);
        } elseif ($request->bg_sort == $sortItem->bg_sort && $request->eng_sort != $sortItem->eng_sort) {
            $validated = $this->validateEngSort($request);
        } else {
            $validated = $this->validateDifferentSorts($request);
        }

        return $validated;
    }

    protected function validateSameSorts(Request $request)
    {
        return $request->validate(
            [
                'bg_sort' => 'required|min:3|max:255|string',
                'eng_sort' => 'required|min:3|max:255|string',
                'column' => 'required|min:3|max:255|string',
                'to_where' => 'required|min:2|max:5|string',
            ]
        );
    }

    protected function validateDifferentSorts(Request $request)
    {
        return $request->validate(
            [
                'bg_sort' => 'required|min:3|max:255|string|unique:sort_items',
                'eng_sort' => 'required|min:3|max:255|string|unique:sort_items',
                'column' => 'required|min:3|max:255|string',
                'to_where' => 'required|min:2|max:5|string',
            ]
        );
    }

    protected function validateBgSort(Request $request)
    {
        return $request->validate(
            [
                'bg_sort' => 'required|min:3|max:255|string|unique:sort_items',
                'eng_sort' => 'required|min:3|max:255|string',
                'column' => 'required|min:3|max:255|string',
                'to_where' => 'required|min:2|max:5|string',
            ]
        );
    }

    protected function validateEngSort(Request $request)
    {
        return $request->validate(
            [
                'bg_sort' => 'required|min:3|max:255|string',
                'eng_sort' => 'required|min:3|max:255|string|unique:sort_items',
                'column' => 'required|min:3|max:255|string',
                'to_where' => 'required|min:2|max:5|string',
            ]
        );
    }

    protected function validateSortBy(Request $request)
    {
        return $request->validate(
            [
                'bg_sort' => 'required|min:3|max:255|string|unique:sort_items',
                'eng_sort' => 'required|min:3|max:255|string|unique:sort_items',
                'column' => 'required|min:3|max:255|string',
                'to_where' => 'required|min:2|max:5|string',
            ]
        );
    }

    protected function getMethodAndBool($slug, Cart $cart = null)
    {
        if($slug == 'acc'){
            $data = [
                'method' => 'isAccepted',
                'bool' => true,
            ];
            $this->updateQuantity($cart, 'sub');
        } elseif ($slug == 'noacc') {
            $data = [
                'method' => 'isAccepted',
                'bool' => false,
            ];
            $this->updateQuantity($cart, 'add');
        } elseif ($slug == 'dlv') {
            $data = [
                'method' => 'isShipped',
                'bool' => true,
            ];
        } elseif ($slug == 'nodlv') {
            $data = [
                'method' => 'isShipped',
                'bool' => false,
            ];
        } elseif ($slug == 'paid') {
            $data = [
                'method' => 'isPaid',
                'bool' => true,
            ];
        } elseif ($slug == 'nopaid') {
            $data = [
                'method' => 'isPaid',
                'bool' => false,
            ];
        } elseif ($slug == 'clo') {
            $data = [
                'method' => 'isClosed',
                'bool' => true,
            ];
        } elseif ($slug == 'noclo') {
            $data = [
                'method' => 'isClosed',
                'bool' => false,
            ];
        } elseif ($slug == 'del') {
            $data = [
                'method' => 'isDeleted',
                'bool' => true,
            ];
        } elseif ($slug == 'res') {
            $data = [
                'method' => 'isDeleted',
                'bool' => false,
            ];
        }

        return $data;
    }

    protected function updateQuantity(Cart $cart, $toWhere)
    {
        foreach ($cart->items as $item)
        {
            $product = Product::findOrFail($item->product_id);
            $warehouse = $product->warehouse;
            $warehouseQuantity = $warehouse->quantity;
            $itemQuantity = $item->quantity;

            if($toWhere == 'subtract') {
                if ($warehouseQuantity < $itemQuantity){
                    $itemQuantity = $warehouseQuantity;
                }
                $new = $warehouseQuantity - $itemQuantity;
            } else {
                $new = $warehouseQuantity + $itemQuantity;
            }

            $movement = Movement::create(
                [
                    'warehouse_id' => $warehouse->id,
                    'to_where' => $toWhere,
                    'with_how_many' => $itemQuantity,
                    'is_buying' => true,
                    'is_warehouse_deleted' => false,
                ]
            );

            Detail::create(
                [
                    'movement_id' => $movement->id,
                    'old' => $warehouseQuantity,
                    'by_how_much' => $itemQuantity,
                    'new' => $new,
                    'to_where' => $toWhere,
                ]
            );

            $warehouse->update(
                [
                    'quantity' => $new,
                    'is_deleted' => false,
                ]
            );

            Buying::create(
                [
                    'cart_id' => $cart->id,
                    'movement_id' => $movement->id
                ]
            );
        }
    }
}
