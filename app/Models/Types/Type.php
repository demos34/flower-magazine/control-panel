<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $guarded = [];

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tags\Tag');
    }

    public function keywords()
    {
        return $this->belongsToMany('App\Models\Tags\Keyword');
    }

    public function categories()
    {
        return $this->hasMany('App\Models\Administrator\Category');
    }

    public function warehouses()
    {
        return $this->hasMany('\App\Models\Cart\Warehouse');
    }
}
