<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('bg_name');
            $table->string('eng_name');
            $table->string('bg_slug')->unique();
            $table->string('eng_slug')->unique();
            $table->longText('bg_description');
            $table->longText('eng_description');
            $table->longText('bg_meta_description');
            $table->longText('eng_meta_description');
            $table->float('price');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('color_id');
            $table->string('image');
            $table->float('size');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')->cascadeOnDelete();
            $table->foreign('color_id')->references('id')->on('colors')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
