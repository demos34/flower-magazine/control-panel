<?php

namespace App\Models\Cart;

use Illuminate\Database\Eloquent\Model;

class Buying extends Model
{
    protected $guarded = [];

    public function movement()
    {
        $this->belongsTo('App\Models\Cart\Movement');
    }

    public function cart()
    {
        $this->belongsTo('App\Models\Cart\Cart');
    }
}
