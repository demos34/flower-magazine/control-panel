<?php

use App\Models\Tags\IndexDescription;
use App\Models\Tags\IndexTag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        IndexDescription::truncate();
        IndexTag::truncate();
        DB::table('index_descriptions')->truncate();
        DB::table('index_tags')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        IndexDescription::create(
            [
                'bg_description' => 'Описание',
                'eng_description' => 'Description',
            ]
        );

        IndexTag::create(
            [
                'bg_tag' => 'Таг 1',
                'eng_tag' => 'Tag 1',
            ]
        );

        IndexTag::create(
            [
                'bg_tag' => 'Таг 2',
                'eng_tag' => 'Tag 2',
            ]
        );

        IndexTag::create(
            [
                'bg_tag' => 'Таг 3',
                'eng_tag' => 'Tag 3',
            ]
        );
    }
}
