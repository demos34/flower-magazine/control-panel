<?php

use App\Models\Administrator\Color;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Color::truncate();
        DB::table('colors')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Color::create(
            [
                'bg_color' => 'Виж всички',
                'eng_color' => 'View All',
            ]
        );
    }
}
