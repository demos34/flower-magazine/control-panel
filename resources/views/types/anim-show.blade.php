@extends('layouts.app')

@section('title')
    Leya | {{$type->name}}
@endsection

@section('custom-css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="animated-cat">
                    <video autoplay="autoplay" muted loop id="animated-cat" playsinline>
                        <source src="{{$type->anim}}" type="video/mp4">
                    </video>
                </div>
            </div>
            <div class="col-md-5">
                <form action="/types/anim/update/{{$type->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="card">
                        <div class="card-header">

                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="anim" class="col-md-4 col-form-label text-md-right">New Animation:</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control-file" id="anim" name="anim">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center m-2">
                        <button type="submit" class="btn btn-outline-primary">
                            Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
