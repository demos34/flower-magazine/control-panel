<?php

use App\Models\Tags\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Tag::truncate();
        DB::table('tags')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Tag::create(
            [
                'bg_tag' => 'Цветя',
                'eng_tag' => 'Flowers',
            ]
        );

        Tag::create(
            [
                'bg_tag' => 'Орхидея',
                'eng_tag' => 'Orchid',
            ]
        );

        Tag::create(
            [
                'bg_tag' => 'Роза',
                'eng_tag' => 'Rose',
            ]
        );

        Tag::create(
            [
                'bg_tag' => 'Лале',
                'eng_tag' => 'Tulip',
            ]
        );

        Tag::create(
            [
                'bg_tag' => 'Опаковки',
                'eng_tag' => 'Packages',
            ]
        );
    }
}
