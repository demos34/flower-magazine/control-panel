<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->id();
            $table->string('names');
            $table->string('address');
            $table->string('phone');
            $table->string('email');
            $table->longText('additional_info')->nullable();
            $table->float('total');
            $table->boolean('isAccepted');
            $table->boolean('isShipped');
            $table->boolean('isPaid');
            $table->boolean('isClosed');
            $table->boolean('isDeleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
