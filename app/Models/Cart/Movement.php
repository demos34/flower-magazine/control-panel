<?php

namespace App\Models\Cart;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    protected $guarded = [];

    public function warehouse()
    {
        return $this->belongsTo('\App\Models\Cart\Warehouse');
    }

    public function details()
    {
        return $this->hasMany('App\Models\Cart\Detail');
    }

    public function buyings()
    {
        return $this->hasMany('App\Models\Cart\Buying');
    }
}
