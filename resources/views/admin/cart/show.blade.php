@extends('layouts.app')

@section('title')
    <title>{{ config('app.name', 'Laravel') }} | Show cart</title>
@endsection


@section('custom-css')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <a href="/administrator/cart">
                    <button class="btn btn-outline-dark">Back</button>
                </a>
            </div>
            <div class="col-md-12 my-3">
                <div class="card">
                    <div class="card-header text-center">
                        <strong>View</strong>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Names</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Created</th>
                                    <th>Accepted?</th>
                                    <th>Delivered?</th>
                                    <th>Paid?</th>
                                    <th>Closed?</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$cart->id}}</td>
                                    <td>{{$cart->names}}</td>
                                    <td>{{$cart->address}}</td>
                                    <td>{{$cart->email}}</td>
                                    <td>{{$cart->created_at}}</td>
                                    <td>@if($cart->isAccepted === 0)
                                            <strong style="color: red">No</strong>
                                            ->
                                            <a href="/administrator/acc/{{$cart->id}}">
                                                <button class="btn btn-outline-success">Accept?</button>
                                            </a>
                                        @else
                                            <strong style="color: green">Yes</strong>
                                            ->
                                            <a href="/administrator/noacc/{{$cart->id}}">
                                                <button class="btn btn-outline-danger">Turn to No?</button>
                                            </a>
                                        @endif
                                    </td>
                                    <td>@if($cart->isShipped === 0)
                                            <strong style="color: red">No</strong>
                                            ->
                                            <a href="/administrator/dlv/{{$cart->id}}">
                                                <button class="btn btn-outline-success">Delivered?</button>
                                            </a>
                                        @else
                                            <strong style="color: green">Yes</strong>
                                            ->
                                            <a href="/administrator/nodlv/{{$cart->id}}">
                                                <button class="btn btn-outline-danger">Turn to No??</button>
                                            </a>
                                        @endif
                                    </td>
                                    <td>@if($cart->isPaid === 0)
                                            <strong style="color: red">No</strong>
                                            ->
                                            <a href="/administrator/paid/{{$cart->id}}">
                                                <button class="btn btn-outline-success">Paid?</button>
                                            </a>
                                        @else
                                            <strong style="color: green">Yes</strong>
                                            ->
                                            <a href="/administrator/nopaid/{{$cart->id}}">
                                                <button class="btn btn-outline-danger">Turn to No??</button>
                                            </a>
                                        @endif
                                    </td>
                                    <td>@if($cart->isClosed === 0)
                                            <strong style="color: red">No</strong>
                                            ->
                                            <a href="/administrator/clo/{{$cart->id}}">
                                                <button class="btn btn-outline-success">Close?</button>
                                            </a>
                                        @else
                                            <strong style="color: green">Yes</strong>
                                            ->
                                            <a href="/administrator/noclo/{{$cart->id}}">
                                                <button class="btn btn-outline-danger">Turn to No??</button>
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($cart->isDeleted === 0)
                                            <a href="/administrator/del/{{$cart->id}}">
                                                <button type="button" class="btn btn-outline-danger" onclick="return confirm('Are Your sure?')">Delete</button>
                                            </a>
                                        @else
                                            <a href="/administrator/res/{{$cart->id}}">
                                                <button type="button" class="btn btn-outline-dark">Restore</button>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card my-3">
                    <div class="card-header text-center">
                        <strong>Summary</strong>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless text-center">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Price per one</th>
                                <th>Quantity</th>
                                <th>Total price for this product</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cart->items as $cartItem)
                                <tr>
                                    <td>{{$cartItem->product->bg_name}} / {{$cartItem->product->eng_name}}</td>
                                    <td>{{$cartItem->product->price}} lv.</td>
                                    <td>{{$cartItem->quantity}}</td>
                                    <td>{{$cartItem->price}} lv.</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <div class="float-right" style="margin-right: 9em">
                            <table class="table table-borderless text-center">
                                <thead>
                                <tr>
                                    <th>Total price for whole cart</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Total: <strong>{{$cart->total}} lv.</strong></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
