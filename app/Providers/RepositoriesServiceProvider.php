<?php

namespace App\Providers;

use App\Repositories\AdminRepository;
use App\Repositories\AdminRepositoryInterface;
use App\Repositories\CartRepository;
use App\Repositories\CartRepositoryInterface;
use App\Repositories\CategoryRepository;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\ColorsRepository;
use App\Repositories\ColorsRepositoryInterface;
use App\Repositories\ProductsRepository;
use App\Repositories\ProductsRepositoryInterface;
use App\Repositories\StorageRepository;
use App\Repositories\StorageRepositoryInterface;
use App\Repositories\TagsRepository;
use App\Repositories\TagsRepositoryInterface;
use App\Repositories\TrafficRepository;
use App\Repositories\TrafficRepositoryInterface;
use App\Repositories\TypesRepository;
use App\Repositories\TypesRepositoryInterface;
use App\Repositories\UserRepository;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\WarehouseRepository;
use App\Repositories\WarehouseRepositoryInterface;
use Illuminate\Support\ServiceProvider;
use Ramsey\Uuid\Type\TypeInterface;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(AdminRepositoryInterface::class, AdminRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(TrafficRepositoryInterface::class, TrafficRepository::class);
        $this->app->bind(TagsRepositoryInterface::class, TagsRepository::class);
        $this->app->bind(ColorsRepositoryInterface::class, ColorsRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);
        $this->app->bind(CartRepositoryInterface::class, CartRepository::class);
        $this->app->bind(ProductsRepositoryInterface::class, ProductsRepository::class);
        $this->app->bind(TypesRepositoryInterface::class, TypesRepository::class);
        $this->app->bind(WarehouseRepositoryInterface::class, WarehouseRepository::class);
    }
}
