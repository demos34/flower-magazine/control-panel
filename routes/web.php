<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/lang/{locale}', function ($locale) {
    if (! in_array($locale, ['en', 'bg'])) {
        $locale = 'en';
//        abort(400);
    }

    session()->put('locale', $locale);
//    dd(session('locale'));
    return redirect()->back();
});

Route::get('/', 'Home\HomeController@index');

//Auth::routes();

/*
 *  Auth routes -> begin
 */

// Authentication Routes...
Route::get('login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('login', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
]);
Route::post('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);

// Password Reset Routes...
Route::post('password/email', [
    'as' => 'password.email',
    'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
]);
Route::get('password/reset', [
    'as' => 'password.request',
    'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
]);
Route::post('password/reset', [
    'as' => 'password.update',
    'uses' => 'Auth\ResetPasswordController@reset'
]);
Route::get('password/reset/{token}', [
    'as' => 'password.reset',
    'uses' => 'Auth\ResetPasswordController@showResetForm'
]);
/*
 * Auth routes -> ends
 */


/*
 * Home Controller
 */
Route::get('/home', 'Home\HomeController@index')->name('home');
Route::get('/about', 'Home\HomeController@about');
Route::get('/contacts', 'Home\HomeController@contacts');


/*
 * User Controller
 */
Route::get('/users', 'User\UsersController@index')->middleware('can:admin-dev')->name('users');
Route::get('/createNew', 'User\UsersController@register')->middleware('can:admin-dev')->name('new-user');
Route::post('/createNew', 'User\UsersController@registerPost')->middleware('can:admin-dev');
Route::get('/users/{user}', 'User\UsersController@show')->middleware('can:admin-dev');
Route::patch('/users/{user}', 'User\UsersController@update')->middleware('can:admin-dev')->name('user-update');
Route::delete('/users/{user}', 'User\UsersController@destroy')->middleware('can:admin-dev')->name(('user-delete'));
Route::patch('/users/password/{user}', 'User\UsersController@changePassword')->middleware('can:admin-dev');


/*
 * Admin Controller
 */
Route::get('/administrator', 'Administrator\AdminController@index')->middleware('can:admin-dev');
Route::get('/sort', 'Administrator\AdminController@sort')->middleware('can:admin-dev');
Route::get('/sort/{sortItem}', 'Administrator\AdminController@sortShow')->middleware('can:admin-dev');
Route::post('/sort', 'Administrator\AdminController@sortStore')->middleware('can:admin-dev');
Route::patch('/sort/{sortItem}', 'Administrator\AdminController@sortUpdate')->middleware('can:admin-dev');
Route::delete('/sort/{sortItem}', 'Administrator\AdminController@sortDestroy')->middleware('can:admin-dev');
/*
 * admin cart in admin controller
 */
Route::get('administrator/cart', 'Administrator\AdminController@cart')->middleware('can:admin-dev');
Route::get('administrator/cart/show/{cart}', 'Administrator\AdminController@cartShow')->middleware('can:admin-dev');
Route::get('administrator/cart/{status}', 'Administrator\AdminController@cartSort')->middleware('can:admin-dev');
Route::get('administrator/{status}/{cart}', 'Administrator\AdminController@cartChangeStatus')->middleware('can:admin-dev');
Route::get('administrator/cart/delete/{cart}', 'Administrator\AdminController@cartDelete')->middleware('can:admin-dev');


/*
 * messages in admin controller
 */
Route::get('messages', 'Administrator\AdminController@messages')->middleware('can:admin-dev');
Route::get('messages/{message}/{cart}', 'Administrator\AdminController@messagesDelete')->middleware('can:admin-dev');

/*
 * Types Controller
 */

Route::get('/types', 'Types\TypesController@index')->middleware('can:admin-dev');
Route::get('/types/{type}', 'Types\TypesController@show')->middleware('can:admin-dev');
Route::post('/types', 'Types\TypesController@store')->middleware('can:admin-dev');
Route::patch('/types/{type}', 'Types\TypesController@update')->middleware('can:admin-dev');
Route::delete('/types/{type}', 'Types\TypesController@destroy')->middleware('can:admin-dev');
Route::get('/types/create/product', 'Types\TypesController@chooseType')->middleware('can:admin-dev');
Route::get('/types/anim/{type}', 'Types\TypesController@anim')->middleware('can:admin-dev');
Route::patch('/types/anim/store/{type}', 'Types\TypesController@animStore')->middleware('can:admin-dev');
Route::get('/types/anim/edit/{type}', 'Types\TypesController@animEdit')->middleware('can:admin-dev');
Route::patch('/types/anim/update/{type}', 'Types\TypesController@animUpdate')->middleware('can:admin-dev');
/*
 * Category controller
 */

Route::get('/category/index/{type}', 'Administrator\CategoryController@index')->middleware('can:admin-dev');
Route::get('/category/show/{category}/{sortItem}', 'Administrator\CategoryController@viewAllProducts')->middleware('can:admin-dev');
Route::get('/category/show/{category}', 'Administrator\CategoryController@viewAllProducts')->middleware('can:admin-dev');
Route::get('/category/cookie/{category}/{sortItem}', 'Administrator\CategoryController@viewCookie')->middleware('can:admin-dev');
Route::post('/category/{type}', 'Administrator\CategoryController@store')->middleware('can:admin-dev');
Route::get('/category/{category}', 'Administrator\CategoryController@show')->middleware('can:admin-dev');
Route::patch('/category/{category}', 'Administrator\CategoryController@update')->middleware('can:admin-dev');
Route::delete('/category/{category}', 'Administrator\CategoryController@destroy')->middleware('can:admin-dev');
Route::get('/category/anim/{category}', 'Administrator\CategoryController@anim')->middleware('can:admin-dev');
Route::patch('/category/anim/store/{category}', 'Administrator\CategoryController@animStore')->middleware('can:admin-dev');
Route::get('/category/anim/edit/{category}', 'Administrator\CategoryController@animEdit')->middleware('can:admin-dev');
Route::patch('/category/anim/update/{category}', 'Administrator\CategoryController@animUpdate')->middleware('can:admin-dev');
/*
 * Category get colors and sort by
 */
Route::get('/category/color/{color}/{category}', 'Administrator\CategoryController@sessionColor')->middleware('can:admin-dev');
Route::get('/category/sort/{sortItem}/{category}', 'Administrator\CategoryController@sessionSort')->middleware('can:admin-dev');

/*
 * Products Controller
 */
Route::get('/products', 'Products\ProductsController@index')->middleware('can:admin-dev');
Route::put('/products/create', 'Products\ProductsController@create')->middleware('can:admin-dev');
Route::get('/products/{product}', 'Products\ProductsController@show')->middleware('can:admin-dev');
Route::get('/products/{product}/edit', 'Products\ProductsController@edit')->middleware('can:admin-dev');
Route::post('/products', 'Products\ProductsController@store')->middleware('can:admin-dev');
Route::patch('/products/{product}', 'Products\ProductsController@update')->middleware('can:admin-dev');
Route::delete('/products/{product}', 'Products\ProductsController@destroy')->middleware('can:admin-dev');
Route::delete('/images/secondary/{image}', 'Products\ProductsController@destroyImage')->middleware('can:admin-dev');

/*
 * Tags Controller
 * finished
 */
Route::get('/seo', 'Seo\TagsController@index')->middleware('can:admin-dev');
Route::post('/index/store', 'Seo\TagsController@storeIndexTags')->middleware('can:admin-dev');
Route::post('/seo/store', 'Seo\TagsController@storeTags')->middleware('can:admin-dev');

Route::get('/seo/{tag}', 'Seo\TagsController@show')->middleware('can:admin-dev');
Route::get('/index/{indexTag}', 'Seo\TagsController@showIndexTag')->middleware('can:admin-dev');
Route::get('/description/{indexDescription}', 'Seo\TagsController@showIndexDescription')->middleware('can:admin-dev');

Route::patch('/seo/{tag}', 'Seo\TagsController@update')->middleware('can:admin-dev');
Route::patch('/index/{indexTag}', 'Seo\TagsController@updateIndexTag')->middleware('can:admin-dev');
Route::patch('/description/{indexDescription}', 'Seo\TagsController@updateIndexDescription')->middleware('can:admin-dev');


Route::delete('/seo/{tag}', 'Seo\TagsController@destroy')->middleware('can:admin-dev');
Route::delete('/index/{indexTag}', 'Seo\TagsController@destroyIndexTag')->middleware('can:admin-dev');

/*
 * Keywords Controller
 */
Route::post('/keywords/store', 'Seo\KeywordsController@store')->middleware('can:admin-dev');
Route::get('/keywords/{keyword}', 'Seo\KeywordsController@show')->middleware('can:admin-dev');
Route::delete('/keywords/{keyword}', 'Seo\KeywordsController@destroy')->middleware('can:admin-dev');
Route::patch('/keywords/{keyword}', 'Seo\KeywordsController@update')->middleware('can:admin-dev');

/*
 * Colors Controller
 */

Route::get('/colors', 'Administrator\ColorsController@index')->middleware('can:admin-dev');
Route::get('/colors/{color}', 'Administrator\ColorsController@show')->middleware('can:admin-dev');
Route::post('/colors/store', 'Administrator\ColorsController@store')->middleware('can:admin-dev');
Route::patch('/colors/{color}', 'Administrator\ColorsController@update')->middleware('can:admin-dev');
Route::delete('/colors/{color}', 'Administrator\ColorsController@destroy')->middleware('can:admin-dev');


/*
 * end of color controller
 */

/*
 * CartController
 */
Route::get('/cart', 'Cart\CartController@index')->middleware('can:admin-dev');
Route::get('/checkout', 'Cart\CartController@checkout')->middleware('can:admin-dev');
Route::post('/cart/{product}', 'Cart\CartController@addToCart')->middleware('can:admin-dev');
Route::put('/cart', 'Cart\CartController@updateCart')->middleware('can:admin-dev');
Route::put('/purchase', 'Cart\CartController@purchase')->middleware('can:admin-dev');
Route::get('/cart/delete/{cartId}', 'Cart\CartController@delete')->middleware('can:admin-dev');


/*
 * Warehouse Controller
 */

Route::get('/warehouse', 'Cart\WarehouseController@index')->middleware('can:admin-dev');
Route::get('/warehouse/{warehouse}', 'Cart\WarehouseController@show')->middleware('can:admin-dev');
Route::patch('/warehouse/qty/{warehouse}', 'Cart\WarehouseController@update')->middleware('can:admin-dev');
Route::get('/warehouse/by/{type}', 'Cart\WarehouseController@view')->middleware('can:admin-dev');
Route::get('/warehouse/all/{type}/{toWhere}/{id}', 'Cart\WarehouseController@allOf')->middleware('can:admin-dev');
Route::put('/warehouse/{warehouse}', 'Cart\WarehouseController@softDelete')->middleware('can:admin-dev');
Route::delete('/warehouse/{warehouse}', 'Cart\WarehouseController@destroy')->middleware('can:admin-dev');


