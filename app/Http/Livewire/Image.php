<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Image extends Component
{
    public $images = [];

    public function increment()
    {
        $this->images[] = count($this->images)+1;
    }

    public function remove($index)
    {
        unset($this->images[$index]);
    }

    public function render()
    {
        return view('livewire.image');
    }
}
