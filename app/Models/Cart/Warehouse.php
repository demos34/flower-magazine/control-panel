<?php

namespace App\Models\Cart;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo('\App\Models\Administrator\Category');
    }

    public function product()
    {
        return $this->belongsTo('\App\Models\Products\Product');
    }

    public function type()
    {
        return $this->belongsTo('\App\Models\Types\Type');
    }

    public function movements()
    {
        return $this->hasMany('\App\Models\Cart\Movement');
    }
}
