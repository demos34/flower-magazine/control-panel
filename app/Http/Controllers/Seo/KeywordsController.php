<?php

namespace App\Http\Controllers\Seo;

use App\Http\Controllers\Controller;
use App\Models\Tags\Keyword;
use App\Repositories\TagsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class KeywordsController extends Controller
{

    private $trafficRepository;
    /**
     * @var TagsRepositoryInterface
     */
    private $tagsRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                TagsRepositoryInterface $tagsRepository)
    {
        $this->middleware('auth');
        $this->trafficRepository = $trafficRepository;
        $this->tagsRepository = $tagsRepository;
    }

    public function index()
    {
        $tags = $this->tagsRepository->getAllTags();
        $indexTags = $this->tagsRepository->getAllIndexTags();
        $description = $this->tagsRepository->getIndexDescription();
        $keywords = $this->tagsRepository->getAllKeywords();

        return redirect('/seo/')->with(
            [
                'tags' => $tags,
                'indexTags' => $indexTags,
                'description' => $description,
                'keywords' => $keywords,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $this->tagsRepository->storeKeyword($request);

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $keyword
     */
    public function show(Keyword $keyword)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return view('seo.keywords.show')->with('keyword', $keyword);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $keyword
     */
    public function update(Request $request, Keyword $keyword)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $update = $this->tagsRepository->updateKeyword($request, $keyword);

        return $this->show($keyword);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $keyword
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keyword $keyword)
    {
        $this->tagsRepository->deleteKeyword($keyword);
        return redirect('/seo/');
    }
}
