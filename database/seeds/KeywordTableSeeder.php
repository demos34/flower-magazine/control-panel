<?php

use App\Models\Tags\Keyword;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KeywordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Keyword::truncate();
        DB::table('keywords')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Keyword::create(
            [
                'bg_keyword' => 'Цветя',
                'eng_keyword' => 'Flowers',
            ]
        );

        Keyword::create(
            [
                'bg_keyword' => 'Орхидея',
                'eng_keyword' => 'Orchid',
            ]
        );

        Keyword::create(
            [
                'bg_keyword' => 'Роза',
                'eng_keyword' => 'Rose',
            ]
        );

        Keyword::create(
            [
                'bg_keyword' => 'Лале',
                'eng_keyword' => 'Tulip',
            ]
        );

        Keyword::create(
            [
                'bg_keyword' => 'Опаковки',
                'eng_keyword' => 'Packages',
            ]
        );
    }
}
