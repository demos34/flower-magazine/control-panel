@extends('layouts.app')


@section('title')
    <title>{{ config('app.name', '') }} | Set Animation</title>
@endsection

@section('custom-css')
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 my-3">
                <div class="well">
                    <h4 class="text-center">
                        <label>
                            Задай анимация за {{$category->eng_name}}
                        </label>
                    </h4>
                    <form action="/category/anim/store/{{$category->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="card">
                            <div class="card-header">

                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="anim" class="col-md-4 col-form-label text-md-right">Animation</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control-file" id="anim" name="anim">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-script')
@endsection
