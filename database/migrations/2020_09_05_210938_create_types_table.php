<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types', function (Blueprint $table) {
            $table->id();
            $table->string('bg_name')->unique();
            $table->string('eng_name')->unique();
            $table->string('bg_slug')->unique();
            $table->string('eng_slug')->unique();
            $table->longText('bg_meta_description')->nullable();
            $table->longText('eng_meta_description')->nullable();
            $table->string('anim')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types');
    }
}
