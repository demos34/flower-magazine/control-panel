<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo('App\Models\Administrator\Category');
    }

    public function images()
    {
        return $this->belongsToMany('App\Models\Products\Image');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tags\Tag');
    }

    public function keywords()
    {
        return $this->belongsToMany('App\Models\Tags\Keyword');
    }

    public function color()
    {
        return $this->belongsTo('App\Models\Administrator\Color');
    }

    public function items()
    {
        return $this->hasMany('App\Models\Cart\Item');
    }

    public function carts()
    {
        return $this->belongsToMany('App\Models\Cart\Cart');
    }

    public function warehouse()
    {
        return $this->hasOne('\App\Models\Cart\Warehouse');
    }
}
