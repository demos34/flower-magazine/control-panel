<?php

namespace App\Repositories;

use App\Models\Administrator\Message;
use App\Models\Administrator\SortItem;
use App\Models\Cart\Cart;
use Illuminate\Http\Request;

interface AdminRepositoryInterface
{
    public function viewAllSortBy();

    public function storeSortBy(Request $request);

    public function updateSortBy(Request $request, SortItem $sortItem);

    public function deleteSortBy(SortItem $sortItem);

    public function getAllUnreadMessages();

    public function setMessagesAsRead($messages);

    public function deleteMessage(Message $message);

    public function getAllAdminCart();

    public function getAllAdminCartSorted($status);

    public function changeStatus($status, Cart $cart);

    public function deleteCart(Cart $cart);
}
