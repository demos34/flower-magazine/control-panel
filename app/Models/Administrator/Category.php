<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tags\Tag');
    }

    public function keywords()
    {
        return $this->belongsToMany('App\Models\Tags\Keyword');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Products\Product');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Types\Type');
    }

    public function warehouses()
    {
        return $this->hasMany('\App\Models\Cart\Warehouse');
    }
}
