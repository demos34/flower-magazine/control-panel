<?php

namespace App\Repositories;

use App\Models\Administrator\Color;
use Illuminate\Http\Request;

interface ColorsRepositoryInterface
{
    public function getAll();

    public function store(Request $request);

    public function update(Request $request, Color $color);

    public function delete(Color $color);
}
