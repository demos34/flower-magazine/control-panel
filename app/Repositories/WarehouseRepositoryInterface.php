<?php

namespace App\Repositories;

use App\Models\Cart\Warehouse;
use Illuminate\Http\Request;

interface WarehouseRepositoryInterface
{
    public function getAllFromWarehouse($isDeleted = null);

    public function getAllTypes();

    public function getAllCategories();

    public function viewBy($type, $isDeleted = null);

    public function viewAllBy($type, $toWhere, $id, $isDeleted = null);

    public function updateQuantity(Request $request, Warehouse $warehouse);
}
