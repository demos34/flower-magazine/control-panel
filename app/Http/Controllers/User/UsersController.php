<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Repositories\TrafficRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class UsersController extends Controller
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    private $trafficRepository;


    public function __construct(UserRepositoryInterface $userRepository,
                                TrafficRepositoryInterface $trafficRepository)
    {
        $this->userRepository = $userRepository;
        $this->trafficRepository = $trafficRepository;
    }

    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $users = $this->userRepository->all();
        return view('admin.users.index')->with('users', $users);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);
        $roles = $this->userRepository->allRoles();

        return view('admin.users.show')->with(
            [
                'user' => $user,
                'roles' => $roles,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $this->userRepository->update($request, $user);

        return $this->show($user);
    }

    public function changePassword(Request $request, User $user)
    {
        $action = 'change password';
        $this->trafficRepository->getRemoteAddress($action);

        $this->userRepository->changePassword($request, $user);
        return $this->index();
    }

    public function register()
    {
        $action = 'register';
        $this->trafficRepository->getRemoteAddress($action);

        return view('admin.users.register');
    }


    public function registerPost(Request $request)
    {
        $action = 'register';
        $this->trafficRepository->getRemoteAddress($action);

        $user = $this->userRepository->register($request);

        return redirect()->route('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $this->userRepository->delete($user);

        return $this->index();
    }
}
