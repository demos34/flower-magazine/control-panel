<?php

namespace App\Models\Cart;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $guarded = [];

    public function movement()
    {
        return $this->belongsTo('App\Models\Cart\Movement');
    }
}
