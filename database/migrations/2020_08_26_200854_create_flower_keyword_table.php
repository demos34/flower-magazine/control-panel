<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlowerKeywordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flower_keyword', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('flower_id');
            $table->unsignedBigInteger('keyword_id');
            $table->timestamps();

            $table->foreign('flower_id')->references('id')->on('flowers')->onDelete('cascade');
            $table->foreign('keyword_id')->references('id')->on('keywords')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flower_keyword');
    }
}
