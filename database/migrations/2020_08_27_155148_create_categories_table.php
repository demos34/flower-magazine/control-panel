<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('bg_name')->unique();
            $table->string('eng_name')->unique();
            $table->string('bg_slug')->unique();
            $table->string('eng_slug')->unique();
            $table->string('anim')->nullable();
            $table->text('bg_meta_description')->nullable();
            $table->text('eng_meta_description')->nullable();
            $table->text('bg_description')->nullable();
            $table->text('eng_description')->nullable();
            $table->unsignedBigInteger('type_id');
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
