<?php
namespace App\Repositories;


use App\Models\Types\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TypesRepository implements TypesRepositoryInterface
{
    public function getAll()
    {
        return Type::all();
    }

    public function storeType(Request $request)
    {
        $validated = $this->validateTypeToStore($request);

        $type = Type::create(
            [
                'bg_name' => $validated['bg_name'],
                'eng_name' => $validated['eng_name'],
                'bg_slug' => $validated['bg_slug'],
                'eng_slug' => $validated['eng_slug'],
                'bg_meta_description' => $validated['bg_meta_description'],
                'eng_meta_description' => $validated['eng_meta_description'],
            ]
        );

        foreach ($validated['tags'] as $tag){
            $type->tags()->attach($tag);
        }

        foreach ($validated['keywords'] as $keyword){
            $type->keywords()->attach($keyword);
        }
        return $msg = [
            'session' => 'success',
            'message' => 'The ' . $type->eng_name . ' is created successfully!',
        ];
    }

    public function updateType(Request $request, Type $type)
    {
        if($this->updateAfterAllOfValidations($request, $type))
        {
            $msg = [
                'session' => 'success',
                'message' => 'The record has successfully updated!',
            ];
        } else {
            $msg = [
                'session' => 'alert',
                'message' => 'Ooops! Something is wrong with: '. $type->eng_name . '!',
            ];
        }

        return $msg;
    }

    public function deleteType(Type $type)
    {
        if($type->delete()){
            $msg = [
                'session' => 'success',
                'message' => 'Type is deleted!',
            ];
        } else {
            $msg = [
                'session' => 'alert',
                'message' => 'Oops! Something get wrong!',
            ];
        }

        return $msg;
    }

    public function animStore(Request $request, Type $type)
    {
        $videoName = $this->validatedAndStoreAnimation($request);
        if ($type->update(['anim' => $videoName])){
            $msg = [
                'session' => 'success',
                'message' => 'The animation is successfully added!',
            ];
        } else {
            $msg = [
                'session' => 'alert',
                'message' => 'Oops! Something get wrong!',
            ];
        }
        return $msg;
    }

    public function animUpdate(Request $request, Type $type)
    {
        $videoName = $this->validatedAndUpdateAnimation($request, $type);
        if ($type->update(['anim' => $videoName])){
            $msg = [
                'session' => 'success',
                'message' => 'The animation is successfully updated!',
            ];
        } else {
            $msg = [
                'session' => 'alert',
                'message' => 'Oops! Something get wrong!',
            ];
        }
        return $msg;
    }

    protected function validatedAndStoreAnimation(Request $request)
    {
        $validated = $this->validateAnimation($request);

        $file = $validated['anim']->getClientOriginalName();
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $date = date('Y-m-d-H-i-s');
        $fileFullName = $date . '-' . $fileName . '.' . $fileExtension;
        $videoName = '/storage/types/anim/' . $fileFullName;
        $request->anim->storeAs('types/anim', $fileFullName, 'public');

        return $videoName;
    }

    protected function validatedAndUpdateAnimation(Request $request, Type $type)
    {
        $this->deleteAnim($type);

        $validated = $this->validateAnimation($request);

        $file = $validated['anim']->getClientOriginalName();
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $date = date('Y-m-d-H-i-s');
        $fileFullName = $date . '-' . $fileName . '.' . $fileExtension;
        $videoName = '/storage/types/anim/' . $fileFullName;
        $request->anim->storeAs('types/anim', $fileFullName, 'public');

        return $videoName;
    }

    protected function deleteAnim(Type $type)
    {
        $originalAnim = $type->anim;
        $array = explode('/', $originalAnim);
        $fileName = $array['4'];
        $path = '/public/' . $array['2'] . '/' . $array['3'] . '/' . $fileName;
        Storage::delete($path);
    }

    protected function validateAnimation(Request $request)
    {
        return $request->validate(
            [
                'anim' => 'required',
            ]
        );
    }

    protected function validateTypeToStore(Request $request)
    {
        return $request->validate(
            [
                'bg_name' => 'required|min:3|max:255|unique:types',
                'eng_name' => 'required|min:3|max:255|unique:types',
                'bg_slug' => 'required|min:3|max:255|unique:types',
                'eng_slug' => 'required|min:3|max:255|unique:types',
                'bg_meta_description' => 'required|min:3|max:1000|',
                'eng_meta_description' => 'required|min:3|max:1000|',
                'tags' => 'array|required|exists:tags,id',
                'keywords' => 'array|required|exists:keywords,id',
            ]
        );
    }

    protected function updateAfterAllOfValidations(Request $request,Type $type)
    {
        $validated = $this->validateUpdateTypes($request);
        $validateNames = $this->prepareToUpdateNames($request, $type);
        $validateSlugs = $this->prepareToUpdateSlugs($request, $type);

        $type->tags()->sync($validated['tags']);
        $type->keywords()->sync($validated['keywords']);

        return $type->update(
            [
                'bg_name' => $validateNames['bg_name'],
                'eng_name' => $validateNames['eng_name'],
                'bg_slug' => $validateSlugs['bg_slug'],
                'eng_slug' => $validateSlugs['eng_slug'],
                'bg_meta_description' => $validated['bg_meta_description'],
                'eng_meta_description' => $validated['eng_meta_description'],
            ]
        );
    }

    protected function validateUpdateTypes(Request $request)
    {
        return $request->validate(
            [
                'bg_meta_description' => 'required|min:3|max:1000|',
                'eng_meta_description' => 'required|min:3|max:1000|',
                'tags' => 'array|required|exists:tags,id',
                'keywords' => 'array|required|exists:keywords,id',
            ]
        );
    }

    protected function prepareToUpdateNames(Request $request, Type $type)
    {
        if($request->bg_name == $type->bg_name && $request->eng_name == $type->eng_name){
            return $this->validateSameTypeNames($request);
        } elseif ($request->bg_name != $type->bg_name && $request->eng_name == $type->eng_name) {
            return $this->validateBgTypeName($request);
        } elseif ($request->bg_name == $type->bg_name && $request->eng_name != $type->eng_name) {
            return $this->validateEngTypeName($request);
        } else {
            return $this->validateDifferentTypeNames($request);
        }
    }

    protected function prepareToUpdateSlugs(Request $request, Type $type)
    {
        if($request->bg_slug == $type->bg_slug && $request->eng_slug == $type->eng_slug){
            return $this->validateSameTypeSlug($request);
        } elseif ($request->bg_slug != $type->bg_slug && $request->eng_slug == $type->eng_slug) {
            return $this->validateBgTypeSlug($request);
        } elseif ($request->bg_slug == $type->bg_slug && $request->eng_slug != $type->eng_slug) {
            return $this->validateEngTypeSlug($request);
        } else {
            return $this->validateDifferentTypeSlug($request);
        }
    }

    protected function validateSameTypeNames(Request $request)
    {
        return $request->validate(
            [
                'bg_name' => 'required',
                'eng_name' => 'required',
            ]
        );
    }

    protected function validateBgTypeName(Request $request)
    {
        return $request->validate(
            [
                'bg_name' => 'required|min:3|max:255|unique:categories',
                'eng_name' => 'required',
            ]
        );
    }

    protected function validateEngTypeName(Request $request)
    {
        return $request->validate(
            [
                'bg_name' => 'required',
                'eng_name' => 'required|min:3|max:255|unique:categories',
            ]
        );
    }

    protected function validateDifferentTypeNames(Request $request)
    {
        return $request->validate(
            [
                'bg_name' => 'required|min:3|max:255|unique:categories',
                'eng_name' => 'required|min:3|max:255|unique:categories',
            ]
        );
    }

    protected function validateSameTypeSlug(Request $request)
    {
        return $request->validate(
            [
                'bg_slug' => 'required',
                'eng_slug' => 'required',
            ]
        );
    }

    protected function validateBgTypeSlug(Request $request)
    {
        return $request->validate(
            [
                'bg_slug' => 'required|min:3|max:255|unique:categories',
                'eng_slug' => 'required',
            ]
        );
    }

    protected function validateEngTypeSlug(Request $request)
    {
        return $request->validate(
            [
                'bg_slug' => 'required',
                'eng_slug' => 'required|min:3|max:255|unique:categories',
            ]
        );
    }



    protected function validateDifferentTypeSlug(Request $request)
    {
        return $request->validate(
            [
                'bg_slug' => 'required|min:3|max:255|unique:categories',
                'eng_slug' => 'required|min:3|max:255|unique:categories',
            ]
        );
    }
}
