@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="alert alert-warning" role="alert">
            Таговете са важни, за да може да се индексира правилно всеки един сайт в търсачките (гугъл, бинг и т.н.) В контролния панел има възможност за добавяне на тагове както към заглавната страница, така и за всеки един продукт. При добавяне на нов продукт (опаковка или цветя), задължително трябва да се изберат всички тагове, които отговарят за сътоветния продукт. Ако няма такива, трябва да се създадат от полето в дясно. Същото важни и за заглавната страница. Там, допълнителното е, че има и описание, което също трябва да се добави.
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card my-3 mx-auto">
                                <div class="card-header">
                                    <h4>
                                        Описание на заглавната страница:
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <p>
                                        <strong>
                                            Описание на български:
                                        </strong>
                                    </p>
                                    <p>
                                        <a href="/description/{{$description->id}}">{{$description->bg_description}}</a>
                                    </p>
                                    <p>
                                        <strong>
                                            Описание на английски:
                                        </strong>
                                    </p>
                                    <p>
                                        <a href="/description/{{$description->id}}">{{$description->eng_description}}</a>
                                    </p>
                                </div>
                            </div>
                            <div class="card my-3 mx-auto">
                                <div class="card-header">
                                    <h4 align="center">
                                        Мета-тагове за заглавната (index page) страница:
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <div class="card-group">
                                        <div class="mx-5">#</div>
                                        <div class="mx-5">Тагове</div>
                                    </div>
                                    <hr>
                                    @if($indexTags->count()>0)
                                        @foreach($indexTags as $tag)
                                            <div class="card-group">
                                                <div class="mx-5">{{$tag->id}}</div>
                                                <div class="mx-5">
                                                    <a href="/index/{{$tag->id}}">{{$tag->bg_tag}} / {{$tag->eng_tag}}</a>
                                                </div>
                                            </div>
                                            <hr>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 my-3">
                            <div class="well">
                                <h4 align="center">
                                    <label>
                                        Добави нов мета-таг
                                    </label>
                                </h4>
                                <form action="/index/store/" method="POST">
                                    @csrf
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="bg_tag">
                                                Bg Tag:
                                            </label>
                                        </div>
                                        <div class="card-body">
                                            <h5>
                                                <input id="bg_tag"
                                                       type="text"
                                                       class="form-control @error('bg_tag') is-invalid @enderror"
                                                       name="bg_tag"
                                                       value="{{ old('bg_tag') }}"
                                                       required autocomplete="bg_index_tag" autofocus>
                                                @error('bg_tag')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </h5>
                                        </div>
                                        <div class="card-header">
                                            <label for="eng_tag">
                                                Eng Tag:
                                            </label>
                                        </div>
                                        <div class="card-body">
                                            <h5>
                                                <input id="eng_tag"
                                                       type="text"
                                                       class="form-control @error('eng_tag') is-invalid @enderror"
                                                       name="eng_tag"
                                                       value="{{ old('eng_tag') }}"
                                                       required autocomplete="eng_index_tag" autofocus>
                                                @error('eng_tag')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center m-2">
                                        <button type="submit" class="btn btn-dark">
                                            Add
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card my-3 mx-auto">
                                <div class="card-header">
                                    <h4 align="center">
                                        Мета-тагове за продукти:
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <div class="card-group">
                                        <div class="mx-5">#</div>
                                        <div class="mx-5">Name</div>
                                    </div>
                                    <hr>
                                    @if($tags->count()>0)
                                        @foreach($tags as $tag)
                                            <div class="card-group">
                                                <div class="mx-5">{{$tag->id}}</div>
                                                <div class="mx-5">
                                                    <a href="/seo/{{$tag->id}}">{{$tag->bg_tag}} / {{$tag->eng_tag}}</a>
                                                </div>
                                            </div>
                                            <hr>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 my-3">
                            <div class="well">
                                <h4 align="center">
                                    <label>
                                        Добави нов мета-таг
                                    </label>
                                </h4>
                                <form action="/seo/store/" method="POST">
                                    @csrf
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="bg_tag">
                                                Bg Tag:
                                            </label>
                                        </div>
                                        <div class="card-body">
                                            <h5>
                                                <input id="bg_tag"
                                                       type="text"
                                                       class="form-control @error('bg_tag') is-invalid @enderror"
                                                       name="bg_tag"
                                                       value="{{ old('bg_tag') }}"
                                                       required autocomplete="bg_tag" autofocus>
                                                @error('bg_tag')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </h5>
                                        </div>
                                        <div class="card-header">
                                            <label for="eng_tag">
                                                Eng Tag:
                                            </label>
                                        </div>
                                        <div class="card-body">
                                            <h5>
                                                <input id="eng_tag"
                                                       type="text"
                                                       class="form-control @error('eng_tag') is-invalid @enderror"
                                                       name="eng_tag"
                                                       value="{{ old('eng_tag') }}"
                                                       required autocomplete="eng_tag" autofocus>
                                                @error('eng_tag')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center m-2">
                                        <button type="submit" class="btn btn-dark">
                                            Add
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card my-3 mx-auto">
                            <div class="card-header">
                                <h4 align="center">
                                    Ключови думи за продукти:
                                </h4>
                            </div>
                            <div class="card-body">
                                <div class="card-group">
                                    <div class="mx-5">#</div>
                                    <div class="mx-5">BG/ENG</div>
                                    <div class="m-auto">Delete</div>
                                </div>
                                <hr>
                                @if($keywords->count()>0)
                                    @foreach($keywords as $keyword)
                                        <div class="card-group">
                                            <div class="mx-5">{{$keyword->id}}</div>
                                            <div class="mx-5">
                                                <a href="/keywords/{{$keyword->id}}">{{$keyword->bg_keyword}} / {{$keyword->eng_keyword}}</a>
                                            </div>
                                            <div class="mx-5">
                                                <form action="/keywords/{{$keyword->id}}/" method="POST">
                                                    @csrf
                                                    @method("DELETE")
                                                    <button type="submit" onclick="confirm('Are you sure?')" class="btn btn-danger">Delete</button>
                                                </form>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 my-3">
                        <div class="well">
                            <h4 align="center">
                                <label>
                                    Добави нова ключова дума
                                </label>
                            </h4>
                            <form action="/keywords/store/" method="POST">
                                @csrf
                                <div class="card">
                                    <div class="card-header">
                                        <label for="bg_keyword">
                                            Bg Keyword:
                                        </label>
                                    </div>
                                    <div class="card-body">
                                        <h5>
                                            <input id="bg_keyword"
                                                   type="text"
                                                   class="form-control @error('bg_keyword') is-invalid @enderror"
                                                   name="bg_keyword"
                                                   value="{{ old('bg_keyword') }}"
                                                   required autocomplete="bg_keyword" autofocus>
                                            @error('bg_keyword')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </h5>
                                    </div>
                                    <div class="card-header">
                                        <label for="eng_keyword">
                                            Eng Keyword:
                                        </label>
                                    </div>
                                    <div class="card-body">
                                        <h5>
                                            <input id="eng_keyword"
                                                   type="text"
                                                   class="form-control @error('eng_keyword') is-invalid @enderror"
                                                   name="eng_keyword"
                                                   value="{{ old('eng_keyword') }}"
                                                   required autocomplete="eng_tag" autofocus>
                                            @error('eng_keyword')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </h5>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-center m-2">
                                    <button type="submit" class="btn btn-dark">
                                        Add
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
