<?php

namespace App\Models\Cart;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded = [];

    public function items()
    {
        return $this->belongsToMany('App\Models\Cart\Item');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Products\Product');
    }

    public function buyings()
    {
        return $this->hasMany('App\Models\Cart\Buying');
    }
}
