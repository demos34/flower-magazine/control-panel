@extends('layouts.app')

@section('title')
    <title>{{ config('app.name', 'Laravel') }} | All categories</title>
@endsection

@section('custom-css')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div>
                    <a href="/types/{{$type->id}}">
                        <button class="btn btn-outline-dark">Back</button>
                    </a>
                </div>
                <div class="card my-3 mx-auto">
                    <div class="card-header">
                        <h4 align="center">
                            <strong>Категория продукти:</strong>
                        </h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Category</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($type->categories->count() > 0)
                                @foreach($type->categories as $category)
                                    <tr>
                                        <td>
                                            {{$category->id}}
                                        </td>
                                        <td>
                                            <strong>
                                                {{$category->bg_name}} / {{$category->eng_name}}
                                            </strong>
                                        </td>
                                        <td>
                                            <img src="{{$category->image}}" style="height: 100px; width: 100px" class="rounded-circle" alt="">
                                        </td>
                                        <td>
                                            <a href="/category/{{$category->id}}">
                                                <button class="btn btn-outline-success float-left">View</button>
                                            </a>
                                            <form action="/category/{{$category->id}}" method="POST" class="float-right">
                                                @csrf
                                                @method('DELETE')
                                                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-outline-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                            <tr>
                                No categories yet!
                            </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-5 my-3">
                <div class="well">
                    <h4 align="center">
                        <label>
                            Добави нова категория
                        </label>
                    </h4>
                    <form action="/category/{{$type->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card">
                            <div class="card-header">

                            </div>
                            <div class="card-body">
                                <label for="bg_name">
                                    BG Name:
                                </label>
                                    <input id="bg_name"
                                           type="text"
                                           class="form-control @error('bg_name') is-invalid @enderror"
                                           name="bg_name"
                                           value="{{ old('bg_name') }}"
                                    required autocomplete="bg_name" autofocus>
                                    @error('bg_name')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                <label for="eng_name">
                                    ENG Name:
                                </label>
                                    <input id="eng_name"
                                           type="text"
                                           class="form-control @error('eng_name') is-invalid @enderror"
                                           name="eng_name"
                                           value="{{ old('eng_name') }}"
                                    required autocomplete="eng_name" autofocus>
                                    @error('eng_name')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                <label for="bg_description">
                                    BG Description:
                                </label>
                                <input id="bg_description"
                                       type="text"
                                       class="form-control @error('bg_description') is-invalid @enderror"
                                       name="bg_description"
                                       value="{{ old('bg_description') }}"
                                       required autocomplete="bg_description" autofocus>
                                @error('bg_description')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label for="eng_description">
                                    ENG Description:
                                </label>
                                <input id="eng_description"
                                       type="text"
                                       class="form-control @error('eng_description') is-invalid @enderror"
                                       name="eng_description"
                                       value="{{ old('eng_description') }}"
                                       required autocomplete="eng_description" autofocus>
                                @error('eng_description')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="card-body">
                                <div class="mx-auto text-center">
                                    <strong>
                                        SEO
                                    </strong>
                                </div>
                                <label for="bg_slug">
                                    BG slug:
                                </label>
                                <input id="bg_slug"
                                       type="text"
                                       class="form-control @error('bg_slug') is-invalid @enderror"
                                       name="bg_slug"
                                       value="{{ old('bg_slug') }}"
                                       required autocomplete="bg_slug" autofocus>
                                @error('bg_slug')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label for="eng_slug">
                                    ENG slug:
                                </label>
                                <input id="eng_slug"
                                       type="text"
                                       class="form-control @error('eng_slug') is-invalid @enderror my-2"
                                       name="eng_slug"
                                       value="{{ old('eng_slug') }}"
                                       required autocomplete="eng_slug" autofocus>
                                @error('eng_slug')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label for="bg_meta_description">
                                    BG Meta Description:
                                </label>
                                <input id="bg_meta_description"
                                       type="text"
                                       class="form-control @error('bg_meta_description') is-invalid @enderror"
                                       name="bg_meta_description"
                                       value="{{ old('bg_meta_description') }}"
                                       required autocomplete="bg_meta_description" autofocus>
                                @error('bg_meta_description')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <label for="eng_meta_description">
                                    ENG meta description:
                                </label>
                                <input id="eng_meta_description"
                                       type="text"
                                       class="form-control @error('eng_meta_description') is-invalid @enderror"
                                       name="eng_meta_description"
                                       value="{{ old('eng_meta_description') }}"
                                       required autocomplete="eng_meta_description" autofocus>
                                @error('eng_meta_description')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label for="tags">
                                    Tags:
                                </label>
                                <select class="js-tags-basic-multiple form-control" name="tags[]" id="tags" multiple="multiple">
                                    @foreach($tags as $tag)
                                        <option value="{{$tag->id}}">{{$tag->bg_tag}} / {{$tag->eng_tag}}</option>
                                    @endforeach
                                </select>
                                    @error('tags')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <div class="card-body">
                                <label for="keywords">
                                    Keywords:
                                </label>
                                <select class="js-keywords-basic-multiple form-control" name="keywords[]" id="keywords" multiple="multiple">
                                    @foreach($keywords as $keyword)
                                        <option value="{{$keyword->id}}">{{$keyword->bg_keyword}} / {{$keyword->eng_keyword}}</option>
                                    @endforeach
                                </select>
                                    @error('keywords')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <div class="mx-auto mb-3 text-center">
                                <strong>
                                    End of SEO
                                </strong>
                            </div>
                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">Image</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control-file" id="image" name="image">
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-script')
    <script src="{{ asset('js/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-tags-basic-multiple').select2();
        });
        $(document).ready(function() {
            $('.js-keywords-basic-multiple').select2();
        });
    </script>
@endsection

