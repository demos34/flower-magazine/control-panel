@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div>
                    <a href="/administrator/">
                        <button class="btn btn-outline-dark">Back</button>
                    </a>
                </div>
                <div class="card my-3 mx-auto">
                    <div class="card-header">
                        <h4 align="center">
                            <strong>Цвят:</strong>
                        </h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Color</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($colors->count() > 0)
                                @foreach($colors as $color)
                                    <tr>
                                        <td>
                                            {{$color->id}}
                                        </td>

                                        <td style="color: {{$color->eng_color}};">
                                            <strong style="@if($color->eng_color == 'white') background-color: black @endif">
                                                {{$color->bg_color}} / {{$color->eng_color}}
                                            </strong>
                                        </td>
                                        <td>
                                            <img src="{{$color->image}}" style="height: 15px; width: 15px; @if($color->eng_color == 'white') background-color: black @endif" class="rounded-circle">
                                        </td>
                                        <td>
                                            <a href="/colors/{{$color->id}}">
                                                <button class="btn btn-outline-success float-left">View</button>
                                            </a>
                                            <form action="/colors/{{$color->id}}" method="POST" class="float-right">
                                                @csrf
                                                @method('DELETE')
                                                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-outline-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                            <tr>
                                No colors yet!
                            </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-3">
                <div class="well">
                    <h4 align="center">
                        <label>
                            Добави нов цвят
                        </label>
                    </h4>
                    <form action="/colors/store" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card">
                            <div class="card-header">
                                <label for="bg_color">
                                    BG Color:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="bg_color"
                                           type="text"
                                           class="form-control @error('bg_color') is-invalid @enderror"
                                           name="bg_color"
                                           value="{{ old('bg_color') }}"
                                    required autocomplete="bg_color" autofocus>
                                    @error('bg_color')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <div class="card-header">
                                <label for="eng_color">
                                    ENG Color:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="eng_color"
                                           type="text"
                                           class="form-control @error('eng_color') is-invalid @enderror"
                                           name="eng_color"
                                           value="{{ old('eng_color') }}"
                                    required autocomplete="eng_color" autofocus>
                                    @error('eng_color')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">Image</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control-file" id="image" name="image">
                                    @if($errors->has('image'))
                                        <strong>{{ $errors->first('image') }}</strong>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

