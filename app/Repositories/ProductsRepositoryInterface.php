<?php

namespace App\Repositories;

use App\Models\Administrator\Category;
use App\Models\Products\Image;
use App\Models\Products\Product;
use Illuminate\Http\Request;

interface ProductsRepositoryInterface
{
    public function all();

    public function getAllCategories(Request $request);

    public function getAllCategoriesFromProductId(Product $product);

    public function allColors();

    public function getAllFromCategory($categoryId);

    public function store(Request $request);

    public function update(Request $request, Product $product);

    public function delete(Product $product);

    public function deleteSecondaryImagesFromController(Image $image);
}
