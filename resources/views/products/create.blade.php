@extends('layouts.app')

@section('title')
    <title>{{ config('app.name', 'Laravel') }} | Create product</title>
@endsection

@section('custom-css')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    @livewireStyles
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-11 my-3">
                <div class="well">
                    <h4 class="text-center">
                        <label>
                            Добави нов продукт
                        </label>
                    </h4>
                    <form action="/products" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card text-center">
                            <div class="card-header">

                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="bg_name">
                                            BG Name:
                                        </label>
                                        <input id="bg_name"
                                               type="text"
                                               class="form-control @error('bg_name') is-invalid @enderror"
                                               name="bg_name"
                                               value="{{ old('bg_name') }}"
                                               required autocomplete="bg_name" autofocus>
                                        @error('bg_name')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="eng_name">
                                            ENG Name:
                                        </label>
                                        <input id="eng_name"
                                               type="text"
                                               class="form-control @error('eng_name') is-invalid @enderror"
                                               name="eng_name"
                                               value="{{ old('eng_name') }}"
                                               required autocomplete="eng_name" autofocus>
                                        @error('eng_name')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="bg_description">
                                            BG Description:
                                        </label>
                                        <input id="bg_description"
                                               type="text"
                                               class="form-control @error('bg_description') is-invalid @enderror"
                                               name="bg_description"
                                               value="{{ old('bg_description') }}"
                                               required autocomplete="bg_description" autofocus>
                                        @error('bg_description')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="eng_description">
                                            ENG Description:
                                        </label>
                                        <input id="eng_description"
                                               type="text"
                                               class="form-control @error('eng_description') is-invalid @enderror"
                                               name="eng_description"
                                               value="{{ old('eng_description') }}"
                                               required autocomplete="eng_description" autofocus>
                                        @error('eng_description')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="color">
                                            Color:
                                        </label>
                                        <select class="form-control" name="color" id="color">
                                            @foreach($colors as $color)
                                                <option value="{{$color->id}}">
                                                        {{$color->bg_color}} / {{$color->eng_color}}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('color')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="category">
                                            Category:
                                        </label>
                                        <select class="form-control" name="category_id" id="category">
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->bg_name}} / {{$category->eng_name}}</option>
                                            @endforeach
                                        </select>
                                        @error('category')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="my-2">
                                    <div class="row text-center justify-content-center">
                                        <div class="col-sm-6">
                                            <label for="price">
                                                Price in lev:
                                            </label>
                                            <input id="price"
                                                   type="number"
                                                   min="0.01"
                                                   step="0.01"
                                                   class="form-control @error('price') is-invalid @enderror"
                                                   name="price"
                                                   value="{{ old('price') }}"
                                                   required autocomplete="price" autofocus>
                                            @error('price')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="size">
                                                Size in cm:
                                            </label>
                                            <input id="size"
                                                   type="number"
                                                   min="0.01"
                                                   step="0.01"
                                                   class="form-control @error('size') is-invalid @enderror"
                                                   name="size"
                                                   value="{{ old('size') }}"
                                                   required autocomplete="size" autofocus>
                                            @error('size')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="my-2">
                                    <div class="row text-center justify-content-center">
                                        <div class="col-sm-6">
                                            <label for="quantity">
                                                Quantity:
                                            </label>
                                            <input id="quantity"
                                                   type="number"
                                                   min="0.01"
                                                   step="0.01"
                                                   class="form-control @error('quantity') is-invalid @enderror"
                                                   name="quantity"
                                                   value="{{ old('quantity') }}"
                                                   required autocomplete="price" autofocus>
                                            @error('quantity')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="metric">
                                                Metric (piece, number, meters, cm):
                                            </label>
                                            <input id="metric"
                                                   type="text"
                                                   class="form-control @error('metric') is-invalid @enderror"
                                                   name="metric"
                                                   value="{{ old('metric') }}"
                                                   required autocomplete="size" autofocus>
                                            @error('metric')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto text-center">
                                    <strong>
                                        SEO
                                    </strong>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="bg_slug">
                                            BG slug:
                                        </label>
                                        <input id="bg_slug"
                                               type="text"
                                               class="form-control @error('bg_slug') is-invalid @enderror"
                                               name="bg_slug"
                                               value="{{ old('bg_slug') }}"
                                               required autocomplete="bg_slug" autofocus>
                                        @error('bg_slug')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="eng_slug">
                                            ENG slug:
                                        </label>
                                        <input id="eng_slug"
                                               type="text"
                                               class="form-control @error('eng_slug') is-invalid @enderror"
                                               name="eng_slug"
                                               value="{{ old('eng_slug') }}"
                                               required autocomplete="eng_slug" autofocus>
                                        @error('eng_slug')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="bg_meta_description">
                                            BG Meta Description:
                                        </label>
                                        <input id="bg_meta_description"
                                               type="text"
                                               class="form-control @error('bg_meta_description') is-invalid @enderror"
                                               name="bg_meta_description"
                                               value="{{ old('bg_meta_description') }}"
                                               required autocomplete="bg_meta_description" autofocus>
                                        @error('bg_meta_description')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="eng_meta_description">
                                            ENG meta description:
                                        </label>
                                        <input id="eng_meta_description"
                                               type="text"
                                               class="form-control @error('eng_meta_description') is-invalid @enderror"
                                               name="eng_meta_description"
                                               value="{{ old('eng_meta_description') }}"
                                               required autocomplete="eng_meta_description" autofocus>
                                        @error('eng_meta_description')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="tags">
                                            Tags:
                                        </label>
                                        <select class="js-tags-basic-multiple form-control" name="tags[]" id="tags" multiple="multiple">
                                            @foreach($tags as $tag)
                                                <option value="{{$tag->id}}">{{$tag->bg_tag}} / {{$tag->eng_tag}}</option>
                                            @endforeach
                                        </select>
                                        @error('tags')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="keywords">
                                            Keywords:
                                        </label>
                                        <select class="js-keywords-basic-multiple form-control" name="keywords[]" id="keywords" multiple="multiple">
                                            @foreach($keywords as $keyword)
                                                <option value="{{$keyword->id}}">{{$keyword->bg_keyword}} / {{$keyword->eng_keyword}}</option>
                                            @endforeach
                                        </select>
                                        @error('keywords')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="mx-auto mb-3" align="center">
                                <strong>
                                    End of SEO
                                </strong>
                            </div>
                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">Primary image</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control-file" id="image" name="image">
                                </div>
                            </div>
                            <div class="form-group">
                                @livewire('image')
                            </div>
                        </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-script')
    @livewireScripts
    <script src="{{ asset('js/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-colors-basic-multiple').select2();
        });
        $(document).ready(function() {
            $('.js-tags-basic-multiple').select2();
        });
        $(document).ready(function() {
            $('.js-keywords-basic-multiple').select2();
        });
    </script>
@endsection

