<?php
namespace App\Repositories;


use App\Models\Administrator\Category;
use App\Models\Cart\Detail;
use App\Models\Cart\Movement;
use App\Models\Cart\Warehouse;
use App\Models\Products\Product;
use App\Models\Types\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WarehouseRepository implements WarehouseRepositoryInterface
{
    public function getAllFromWarehouse($isDeleted = null)
    {
        return Warehouse::where('is_deleted', $this->isDeleted($isDeleted))->paginate(15);
    }

    public function getAllTypes()
    {
        return Type::all();
    }

    public function getAllCategories()
    {
        return Category::all();
    }

    public function viewBy($type, $isDeleted = null)
    {
        $method = $this->getMethod($type);
        if(!isset($method['order'])){
            return $method;
        }
        return Warehouse::where('is_deleted', $this->isDeleted($isDeleted))->orderBy($method['order'], $method['to_where'])->paginate(15);
    }

    public function viewAllBy($type, $toWhere, $id, $isDeleted = null)
    {
        return Warehouse::where('is_deleted', $this->isDeleted($isDeleted))->where($type.'_id', $id)->orderBy('created_at', $toWhere)->paginate(15);
    }

    public function updateQuantity(Request $request, Warehouse $warehouse)
    {
        $old = $warehouse->quantity;
        $with = (float)$request->quantity;
        $new = $this->checkQuantity($old, $with, $request);
        if($new < 0){
            return $msg = [
                'session' => 'alert',
                'message' => 'Not enough of ' . $warehouse->product->eng_name . ' in warehouse! Max quantity is: ' . $old,
            ];
        }

        $movement = Movement::create(
            [
                'warehouse_id' => $warehouse->id,
                'to_where' => $request->move_to,
                'with_how_many' => $request->quantity,
                'is_buying' => false,
                'is_warehouse_deleted' => false,
            ]
        );

        Detail::create(
            [
                'movement_id' => $movement->id,
                'old' => $old,
                'by_how_much' => $request->quantity,
                'new' => $new,
                'to_where' => $request->move_to,
            ]
        );

        $warehouse->update(
            [
                'quantity' => $new,
                'is_deleted' => false,
            ]
        );

        return $msg = [
            'session' => 'success',
            'message' => 'The record is successfully updated!',
        ];
    }

    protected function getMethod($type)
    {
        $method = [];
        if($type == 'typeasc'){
            $method = [
                'order' => 'type_id',
                'to_where' => 'asc',
            ];

        } elseif ($type == 'typedesc') {
            $method = [
                'order' => 'type_id',
                'to_where' => 'desc',
            ];
        } elseif ($type == 'categoryasc') {
            $method = [
                'order' => 'category_id',
                'to_where' => 'asc',
            ];

        } elseif ($type == 'categorydesc') {
            $method = [
                'order' => 'category_id',
                'to_where' => 'desc',
            ];

        } elseif ($type == 'productasc') {
            $method = [
                'order' => 'product_id',
                'to_where' => 'asc',
            ];

        } elseif ($type == 'productdesc') {
            $method = [
                'order' => 'product_id',
                'to_where' => 'desc',
            ];

        } elseif ($type == 'quantityasc') {
            $method = [
                'order' => 'quantity',
                'to_where' => 'asc',
            ];

        } elseif ($type == 'quantitydesc') {
            $method = [
                'order' => 'quantity',
                'to_where' => 'desc',
            ];

        } elseif ($type == 'nameasc') {
            return $this->viewSortedByProductName('asc');
        } elseif ($type == 'namedesc') {
            return $this->viewSortedByProductName('Desc');
        } elseif ($type == 'allasc') {
            $method = [
                'order' => 'id',
                'to_where' => 'asc',
            ];

        } elseif ($type == 'alldesc') {
            $method = [
                'order' => 'id',
                'to_where' => 'desc',
            ];

        }

        return $method;

    }

    protected function viewSortedByProductName($toWhere)
    {
        return Warehouse::select('warehouses.*')
            ->join('products', 'products.id', '=', 'warehouses.product_id')
            ->where('is_deleted', '=', false)
            ->orderBy('products.eng_name', $toWhere)
            ->paginate(2);
    }

    protected function isDeleted($isDeleted)
    {
        if($isDeleted === NULL){
            return FALSE;
        }
        return TRUE;
    }

    protected function checkQuantity($old, $with, Request $request)
    {
        if ($request->move_to == 'add') {
            $new = $old + $with;
        } else {
            if ($old > $with) {
                $new = $old - $with;
            } else {
                $new = $old - $with;
            }
        }
        return $new;
    }
}
