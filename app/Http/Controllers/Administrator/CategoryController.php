<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Administrator\Category;
use App\Models\Administrator\Color;
use App\Models\Administrator\SortItem;
use App\Models\Types\Type;
use App\Repositories\CategoryRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository, CategoryRepositoryInterface $categoryRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Type $type
     */
    public function index(Type $type)
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

//        $categories = $this->categoryRepository->getAll();
        $tags = $this->categoryRepository->getAllTags();
        $keywords = $this->categoryRepository->getAllKeywords();


        return view('admin.categories.index')->with(
            [
//                'categories' => $categories,
                'tags' => $tags,
                'keywords' => $keywords,
                'type' => $type
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @param  Type  $type
     */
    public function store(Request $request, Type $type)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->categoryRepository->store($request, $type);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  $category
     * @return Response
     */
    public function show(Category $category)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        $tags = $this->categoryRepository->getAllTags();
        $keywords = $this->categoryRepository->getAllKeywords();

        return view('admin.categories.show')->with(
            [
                'category' => $category,
                'tags' => $tags,
                'keywords' => $keywords,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param   $category
     */
    public function update(Request $request, Category $category)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->categoryRepository->update($request, $category);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  $category
     * @param SortItem $sortItem
     */
    public function viewCookie(Category $category, SortItem $sortItem)
    {
        $action = 'view Cookie';
        $this->trafficRepository->getRemoteAddress($action);

//        $this->categoryRepository->cookie($sortItem);

//        return $this->viewAllProducts($category, $sortItem);
    }

    protected function checkCookie(Category $category, SortItem $sortItem)
    {
//        return $this->viewAllProducts($category, $sortItem);
    }

    /**
     * Display the specified resource.
     *
     * @param Color $color
     * @param Category $category
     */
    public function sessionColor(Color $color, Category $category)
    {
        $action = 'sessionColor';
        $this->trafficRepository->getRemoteAddress($action);
        Session::put('color', $color->id);
        return redirect('category/show/'.$category->id);
    }

    /**
     * Display the specified resource.
     *
     * @param SortItem $sortItem
     * @param Category $category
     */
    public function sessionSort(SortItem $sortItem, Category $category)
    {
        $action = 'sessionSort';
        $this->trafficRepository->getRemoteAddress($action);

        Session::put('sort', $sortItem->id);
        return redirect('category/show/'.$category->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Category $category
     */
    public function viewAllProducts(Category $category)
    {
        $action = 'viewAllProducts';
        $this->trafficRepository->getRemoteAddress($action);

        $sortedProducts = $this->categoryRepository->getAllProductsSortedBy($category);
        $tags = $this->categoryRepository->getAllTags();
        $keywords = $this->categoryRepository->getAllKeywords();
        $sort = $this->categoryRepository->getAllSortBy();
        $colors = $this->categoryRepository->getAllColors();

        return view('admin.categories.all')->with(
            [
                'category' => $category,
                'tags' => $tags,
                'keywords' => $keywords,
                'sort' => $sort,
                'products' => $sortedProducts,
                'colors' => $colors,
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     */
    public function destroy(Category $category)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->categoryRepository->delete($category);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Color $color
     * @param Category $category
     */
    public function test(Color $color, Category $category)
    {
       Session::put('color', $color->id);
       return $this->test2($category);

    }

    public function test2(Category $category)
    {
        dd(Session::has('color1'));

        dd(Session::get('color'));
    }

    public function anim(Category $category)
    {
        $action = 'anim';
        $this->trafficRepository->getRemoteAddress($action);

        return view('admin.categories.anim')->with(
            [
                'category' => $category,
            ]
        );
    }

    public function animStore(Request $request, Category $category)
    {
        $action = 'animStore';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->categoryRepository->animStore($request, $category);
        return redirect('/category/'.$category->id)->with($msg['session'], $msg['message']);
    }

    public function animEdit(Category $category)
    {
        $action = 'animStore';
        $this->trafficRepository->getRemoteAddress($action);

        return view('admin.categories.anim-show')->with(
            [
                'category' => $category,
            ]
        );
    }

    public function animUpdate(Request $request, Category $category)
    {
        $action = 'animStore';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->categoryRepository->animUpdate($request, $category);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

}
