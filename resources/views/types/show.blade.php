@extends('layouts.app')


@section('title')
    <title>{{ config('app.name', '') }} | {{$type->eng_name}} | {{$type->bg_name}}</title>
@endsection

@section('custom-css')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="py-2">
                    <a href="/types/">
                        <button class="btn btn-outline-primary">Back</button>
                    </a>
                    <a href="/category/index/{{$type->id}}" class="float-right">
                        <button class="btn btn-outline-success">View all categories</button>
                    </a>
                </div>
                <div class="card">
                    <div class="card-header text-center">
                        Тип категория:
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>#</td>
                                <td>{{$type->id}}</td>
                            </tr>
                            <tr>
                                <td>Category:</td>
                                <td>{{$type->bg_name}} / {{$type->eng_name}}</td>
                            </tr>

                            <tr>
                                <td>Animation:</td>
                                <td>
                                    @if($type->anim === NULL)
                                        <strong style="color: red">No</strong>
                                        -> <a href="/types/anim/{{$type->id}}"><strong>Set new animation</strong></a>
                                    @else
                                        <strong style="color: green">Yes</strong>
                                        -> <a href="/types/anim/edit/{{$type->id}}"><strong> View and Update existing animation</strong></a>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Slug</td>
                                <td>{{$type->bg_slug}} / {{$type->eng_slug}}</td>
                            </tr>
                            <tr>
                                <td>SEO Description</td>
                                <td>{{$type->bg_meta_description}} / {{$type->eng_meta_description}}</td>
                            </tr>
                            <tr>
                                <td>SEO Tags</td>
                                <td>
                                    @foreach($type->tags as $tag)
                                        <a href="/seo/{{$tag->id}}" style="text-decoration: none; color: black">
                                            <span class="m-1 px-1"
                                                   style="background-color: gray; border-radius: 5%;">
                                                {{$tag->bg_tag}}
                                            </span> /
                                            <span class="m-1 px-1"
                                                   style="background-color: gray; border-radius: 5%;">
                                                {{$tag->eng_tag}}
                                            </span>
                                        </a>
                                        <br>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>SEO Keywords</td>
                                <td>
                                    @foreach($type->keywords as $keyword)
                                        <a href="/keywords/{{$keyword->id}}" style="text-decoration: none; color: black">
                                            <span class="m-1 px-1"
                                                   style="background-color: gray; border-radius: 5%;">
                                                {{$keyword->bg_keyword}}
                                            </span> /
                                            <span class="m-1 px-1"
                                                   style="background-color: gray; border-radius: 5%;">
                                                {{$keyword->eng_keyword}}
                                            </span>
                                        </a>
                                        <br>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>Delete</td>
                                <td>
                                    <form action="/category/{{$type->id}}" method="POST" class="float-left">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" onclick="return confirm('Are you sure?')"
                                                class="btn btn-outline-danger">Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6 my-3">
                <div class="well">
                    <h4 align="center">
                        <label>
                            Промени тип:
                        </label>
                    </h4>
                    <form action="/types/{{$type->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="card">
                            <div class="card-header">

                            </div>
                            <div class="card-body">
                                <label for="bg_name">
                                    BG Name:
                                </label>
                                    <input id="bg_name"
                                           type="text"
                                           class="form-control @error('bg_name') is-invalid @enderror"
                                           name="bg_name"
                                           value=@if(old('bg_name'))"{{ old('bg_name') }}"@else"{{ $type->bg_name }}"@endif
                                           required autocomplete="bg_name" autofocus>
                                    @error('bg_name')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                <label for="eng_name">
                                    ENG Name:
                                </label>
                                    <input id="eng_name"
                                           type="text"
                                           class="form-control @error('eng_name') is-invalid @enderror"
                                           name="eng_name"
                                           value=@if(old('eng_name'))"{{ old('eng_name') }}"@else"{{ $type->eng_name }}"@endif
                                           required autocomplete="eng_name" autofocus>
                                    @error('eng_name')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <div class="card-body">
                                <div class="mx-auto" align="center">
                                    <strong>
                                        SEO
                                    </strong>
                                </div>
                                <label for="bg_slug">
                                    BG slug:
                                </label>
                                <input id="bg_slug"
                                       type="text"
                                       class="form-control @error('bg_slug') is-invalid @enderror"
                                       name="bg_slug"
                                       value=@if(old('bg_slug'))"{{ old('bg_slug') }}"@else"{{ $type->bg_slug }}"@endif
                                       required autocomplete="bg_slug" autofocus>
                                @error('bg_slug')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label for="eng_slug">
                                    ENG slug:
                                </label>
                                <input id="eng_slug"
                                       type="text"
                                       class="form-control @error('eng_slug') is-invalid @enderror my-2"
                                       name="eng_slug"
                                       value=@if(old('eng_slug'))"{{ old('eng_slug') }}"@else"{{ $type->eng_slug }}"@endif
                                       required autocomplete="eng_slug" autofocus>
                                @error('eng_slug')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label for="bg_meta_description">
                                    BG Meta Description:
                                </label>
                                <input id="bg_meta_description"
                                       type="text"
                                       class="form-control @error('bg_meta_description') is-invalid @enderror"
                                       name="bg_meta_description"
                                       value=@if(old('bg_meta_description'))"{{ old('bg_meta_description') }}"@else"{{ $type->bg_meta_description }}"@endif
                                       required autocomplete="bg_meta_description" autofocus>
                                @error('bg_meta_description')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <label for="eng_meta_description">
                                    ENG meta description:
                                </label>
                                <input id="eng_meta_description"
                                       type="text"
                                       class="form-control @error('eng_meta_description') is-invalid @enderror"
                                       name="eng_meta_description"
                                       value=@if(old('eng_meta_description'))"{{ old('eng_meta_description') }}"@else"{{ $type->eng_meta_description }}"@endif
                                       required autocomplete="eng_meta_description" autofocus>
                                @error('eng_meta_description')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label for="tags">
                                    Tags:
                                </label>
                                <select class="js-tags-basic-multiple form-control" name="tags[]" id="tags" multiple="multiple">
                                    @foreach($tags as $tag)
                                        <option value="{{$tag->id}}">{{$tag->bg_tag}} / {{$tag->eng_tag}}</option>
                                    @endforeach
                                </select>
                                @error('tags')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="card-body">
                                <label for="keywords">
                                    Keywords:
                                </label>
                                <select class="js-keywords-basic-multiple form-control" name="keywords[]" id="keywords" multiple="multiple">
                                    @foreach($keywords as $keyword)
                                        <option value="{{$keyword->id}}">{{$keyword->bg_keyword}} / {{$keyword->eng_keyword}}</option>
                                    @endforeach
                                </select>
                                @error('keywords')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mx-auto mb-3 text-center">
                                <strong>
                                    End of SEO
                                </strong>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-script')
    <script src="{{ asset('js/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-tags-basic-multiple').select2();

        });
        $('.js-tags-basic-multiple').select2().val({!! json_encode($type->tags()->get()->pluck('id')->toArray()) !!}).trigger('change');
        $(document).ready(function() {
            $('.js-keywords-basic-multiple').select2();
        });
        $('.js-keywords-basic-multiple').select2().val({!! json_encode($type->keywords()->get()->pluck('id')->toArray()) !!}).trigger('change');
    </script>
@endsection
