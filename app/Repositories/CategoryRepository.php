<?php

namespace App\Repositories;


use App\Models\Administrator\Category;
use App\Models\Administrator\Color;
use App\Models\Administrator\SortItem;
use App\Models\Products\Product;
use App\Models\Tags\Keyword;
use App\Models\Tags\Tag;
use App\Models\Types\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CategoryRepository implements CategoryRepositoryInterface
{
    const COOKIE_VALUE = 1;

    public function getAll()
    {
        return Category::all();
    }

    public function getAllSortBy()
    {
        return SortItem::all();
    }

    public function getAllProductsSortedBy(Category $category)
    {
        $sortItem = $this->emptySessionSort();
        $color = $this->emptySessionColor();

        $this->cookie($sortItem, $color);
        if($color->id == 1){
            $product = Product::where(
                [
                    'category_id' => $category->id,
                ]
            )->orderBy($sortItem->column, $sortItem->to_where)->get();
        } else {
            $product = Product::where(
                [
                    'category_id' => $category->id,
                    'color_id' => $color->id,
                ]
            )->orderBy($sortItem->column, $sortItem->to_where)->get();
        }
        return $product;
    }

    public function getAllColors()
    {
        return Color::all();
    }

    public function getAllTags()
    {
        return Tag::all();
    }

    public function getAllKeywords()
    {
        return Keyword::all();
    }

    public function store(Request $request, Type $type)
    {
        $validated = $this->validateCategoryToStore($request);
        $imagePath = $this->storeImage($request);

        $category = Category::create(
            [
                'bg_name' => $validated['bg_name'],
                'eng_name' => $validated['eng_name'],
                'bg_description' => $validated['bg_description'],
                'eng_description' => $validated['eng_description'],
                'bg_slug' => $validated['bg_slug'],
                'eng_slug' => $validated['eng_slug'],
                'bg_meta_description' => $validated['bg_meta_description'],
                'eng_meta_description' => $validated['eng_meta_description'],
                'image' => $imagePath,
                'type_id' => $type->id,
            ]
        );

        foreach ($validated['tags'] as $tag){
            $category->tags()->attach($tag);
        }

        foreach ($validated['keywords'] as $keyword){
            $category->keywords()->attach($keyword);
        }

        return $msg = [
            'session' => 'success',
            'message' => ucfirst($category->eng_name) . ' is successfully created!',
        ];

    }

    public function update(Request $request, Category $category)
    {
        if ($request->hasFile('image') === TRUE){
            $imagePath = $this->storeImage($request);
            $this->deleteImageFromStorage($category);
            $category->update(
                [
                    'image' => $imagePath,
                ]
            );
        }
        if($this->updateAfterAllOfValidations($request, $category)){
            $msg = [
                'session' => 'success',
                'message' => 'The category ' . $category->eng_name . ' is successfully updated!',
            ];
        } else {
            $msg = [
                'session' => 'alert',
                'message' => 'Oops! Something is wrong with update of category ' . $category->eng_name . '!',
            ];
        }

        return $msg;
    }

    public function delete(Category $category)
    {
        $this->deleteImageFromStorage($category);
        if($category->delete()){
            $msg = [
                'session' => 'success',
                'message' => 'The category is successfully deleted!',
            ];
        } else {
            $msg = [
                'session' => 'alert',
                'message' => 'Oops! Something get wrong!',
            ];
        }
        return $msg;
    }

    public function animStore(Request $request, Category $category)
    {
        $videoName = $this->validatedAndStoreAnimation($request);
        if ($category->update(['anim' => $videoName])){
            $msg = [
                'session' => 'success',
                'message' => 'The animation is successfully added!',
            ];
        } else {
            $msg = [
                'session' => 'alert',
                'message' => 'Oops! Something get wrong!',
            ];
        }
        return $msg;
    }

    public function animUpdate(Request $request, Category $category)
    {
        $videoName = $this->validatedAndUpdateAnimation($request, $category);
        if ($category->update(['anim' => $videoName])){
            $msg = [
                'session' => 'success',
                'message' => 'The animation is successfully updated!',
            ];
        } else {
            $msg = [
                'session' => 'alert',
                'message' => 'Oops! Something get wrong!',
            ];
        }
        return $msg;
    }

    protected function validatedAndStoreAnimation(Request $request)
    {
        $validated = $this->validateAnimation($request);

        $file = $validated['anim']->getClientOriginalName();
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $date = date('Y-m-d-H-i-s');
        $fileFullName = $date . '-' . $fileName . '.' . $fileExtension;
        $videoName = '/storage/categories/anim/' . $fileFullName;
        $request->anim->storeAs('categories/anim', $fileFullName, 'public');

        return $videoName;
    }

    protected function validatedAndUpdateAnimation(Request $request, Category $category)
    {
        $this->deleteAnim($category);

        $validated = $this->validateAnimation($request);

        $file = $validated['anim']->getClientOriginalName();
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $date = date('Y-m-d-H-i-s');
        $fileFullName = $date . '-' . $fileName . '.' . $fileExtension;
        $videoName = '/storage/categories/anim/' . $fileFullName;
        $request->anim->storeAs('categories/anim', $fileFullName, 'public');

        return $videoName;
    }

    protected function deleteAnim(Category $category)
    {
        $originalAnim = $category->anim;
        $array = explode('/', $originalAnim);
        $fileName = $array['4'];
        $path = '/public/' . $array['2'] . '/' . $array['3'] . '/' . $fileName;
        Storage::delete($path);
    }

    protected function validateAnimation(Request $request)
    {
        return $request->validate(
            [
                'anim' => 'required',
            ]
        );
    }

    private function validateCategoryToStore(Request $request)
    {
        return $request->validate(
            [
                'bg_name' => 'required|min:3|max:255|unique:categories',
                'eng_name' => 'required|min:3|max:255|unique:categories',
                'bg_description' => 'required|min:3|max:1000|',
                'eng_description' => 'required|min:3|max:1000|',
                'bg_slug' => 'required|min:3|max:255|unique:categories',
                'eng_slug' => 'required|min:3|max:255|unique:categories',
                'bg_meta_description' => 'required|min:3|max:1000|',
                'eng_meta_description' => 'required|min:3|max:1000|',
                'image' => 'required|image',
                'tags' => 'array|required|exists:tags,id',
                'keywords' => 'array|required|exists:keywords,id',
            ]
        );
    }

    private function storeImage(Request $request)
    {
        $validatedImage = $this->validateImage($request);

        $file = $validatedImage['image']->getClientOriginalName();
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $date = date('Y-m-d-H-i-s');
        $fileFullName = $date . '-' . $fileName . '.' . $fileExtension;
        $imagePath = '/storage/categories/' . $fileFullName;
        $request->image->storeAs('categories', $fileFullName, 'public');

//        $destionationPath = "C:/xampp/htdocs/leya/storage/app/public/categories";
//        $request->image->move($destionationPath, $fileFullName);
        return $imagePath;
    }

    private function validateImage(Request $request)
    {
        return $request->validate(
            [
                'image' => 'image',
            ]
        );
    }

    private function prepareToUpdateNames(Request $request, Category $category)
    {
        if($request->bg_name == $category->bg_name && $request->eng_name == $category->eng_name){
            return $this->validateSameCategoryName($request);
        } elseif ($request->bg_name != $category->bg_name && $request->eng_name == $category->eng_name) {
            return $this->validateBgCategoryName($request);
        } elseif ($request->bg_name == $category->bg_name && $request->eng_name != $category->eng_name) {
            return $this->validateEngCategoryName($request);
        } else {
            return $this->validateDifferentCategoryNames($request);
        }
    }

    private function prepareToUpdateSlugs(Request $request, Category $category)
    {
        if($request->bg_slug == $category->bg_slug && $request->eng_slug == $category->eng_slug){
            return $this->validateSameCategorySlug($request);
        } elseif ($request->bg_slug != $category->bg_slug && $request->eng_slug == $category->eng_slug) {
            return $this->validateBgCategorySlug($request);
        } elseif ($request->bg_slug == $category->bg_slug && $request->eng_slug != $category->eng_slug) {
            return $this->validateEngCategorySlug($request);
        } else {
            return $this->validateDifferentCategorySlug($request);
        }
    }

    private function validateUpdateCategory(Request $request)
    {
        return $request->validate(
            [
                'bg_description' => 'required|min:3|max:1000|',
                'eng_description' => 'required|min:3|max:1000|',
                'bg_meta_description' => 'required|min:3|max:1000|',
                'eng_meta_description' => 'required|min:3|max:1000|',
                'image' => 'image',
                'tags' => 'array|required|exists:tags,id',
                'keywords' => 'array|required|exists:keywords,id',
            ]
        );
    }

    private function validateSameCategoryName(Request $request)
    {
        return $request->validate(
            [
                'bg_name' => 'required',
                'eng_name' => 'required',
            ]
        );
    }

    private function validateBgCategoryName(Request $request)
    {
        return $request->validate(
            [
                'bg_name' => 'required|min:3|max:255|unique:categories',
                'eng_name' => 'required',
            ]
        );
    }

    private function validateEngCategoryName(Request $request)
    {
        return $request->validate(
            [
                'bg_name' => 'required',
                'eng_name' => 'required|min:3|max:255|unique:categories',
            ]
        );
    }

    private function validateDifferentCategoryNames(Request $request)
    {
        return $request->validate(
            [
                'bg_name' => 'required|min:3|max:255|unique:categories',
                'eng_name' => 'required|min:3|max:255|unique:categories',
            ]
        );
    }

    private function validateSameCategorySlug(Request $request)
    {
        return $request->validate(
            [
                'bg_slug' => 'required',
                'eng_slug' => 'required',
            ]
        );
    }

    private function validateBgCategorySlug(Request $request)
    {
        return $request->validate(
            [
                'bg_slug' => 'required|min:3|max:255|unique:categories',
                'eng_slug' => 'required',
            ]
        );
    }

    private function validateEngCategorySlug(Request $request)
    {
        return $request->validate(
            [
                'bg_slug' => 'required',
                'eng_slug' => 'required|min:3|max:255|unique:categories',
            ]
        );
    }



    private function validateDifferentCategorySlug(Request $request)
    {
        return $request->validate(
            [
                'bg_slug' => 'required|min:3|max:255|unique:categories',
                'eng_slug' => 'required|min:3|max:255|unique:categories',
            ]
        );
    }

    private function deleteImageFromStorage(Category $category)
    {
        $originalImage = $category->image;
        $array = explode('/', $originalImage);
        $fileName = $array['3'];
//        $destionationPath = "C:/xampp/htdocs/leya/storage/app/public/categories/" . $fileName;
//        unlink($destionationPath);
        $path = '/public/' . $array['2'] . '/' . $fileName;
        Storage::delete($path);
    }

    private function updateAfterAllOfValidations(Request $request,Category $category)
    {
        $validated = $this->validateUpdateCategory($request);
        $validateNames = $this->prepareToUpdateNames($request, $category);
        $validateSlugs = $this->prepareToUpdateSlugs($request, $category);

        $category->tags()->sync($validated['tags']);
        $category->keywords()->sync($validated['keywords']);

        return $category->update(
            [
                'bg_name' => $validateNames['bg_name'],
                'eng_name' => $validateNames['eng_name'],
                'bg_description' => $validated['bg_description'],
                'eng_description' => $validated['eng_description'],
                'bg_slug' => $validateSlugs['bg_slug'],
                'eng_slug' => $validateSlugs['eng_slug'],
                'bg_meta_description' => $validated['bg_meta_description'],
                'eng_meta_description' => $validated['eng_meta_description'],
            ]
        );
    }

    protected function cookie(SortItem $sortItem, Color $color)
    {
//        $cookie = Cookie::get();
//        if(empty($cookie['sort'])){
//            $update = Cookie::queue('sort', (int)$sortItem->id, '3600');
//        } else {
//            dd('not empty');
//        }
            Cookie::queue('sort', (int)$sortItem->id, '3600');
            Cookie::queue('color', (int)$color->id, '3600');

    }

    protected function validator(Request $request)
    {
        return $request->validate(
            [
                'sort' => 'exists:sort_items,id',
            ]
        );
    }

    protected function emptyRequest(Request $request)
    {
        if($request->has('sort') === TRUE){
            $validated = $this->validator($request);
            $sortItem = SortItem::find((int)$validated['sort']);
        } else {
            $cookie = Cookie::get();
            if(empty($cookie['sort'])){
                $sortItem = SortItem::find(1)->firstOrFail();
            } else {
                $sortItem = SortItem::find((int)$cookie['sort']);
            }
        }

        return $sortItem;
    }

    protected function sortValidator()
    {
        $data = [
            'sort' => Session::get('sort'),
        ];

        return Validator::make($data, [
            'sort' => 'exists:sort_items,id',
        ])->validate();
    }

    protected function colorValidator()
    {
        $data = [
            'color' => Session::get('color'),
        ];

        return Validator::make($data, [
            'color' => 'exists:colors,id',
        ])->validate();
    }

    protected function emptySessionSort()
    {
        if(Session::has('sort')){
            $validated = $this->sortValidator();
            $sortItem = SortItem::find((int)$validated['sort']);
        } else {
            $cookie = Cookie::get();
            if(empty($cookie['sort'])){
                $sortItem = SortItem::find(1)->firstOrFail();
            } else {
                $sortItem = SortItem::find((int)$cookie['sort']);
            }
        }
        return $sortItem;
    }

    protected function emptySessionColor()
    {
        if(Session::has('color')){
            $validated = $this->colorValidator();
            $color = Color::find((int)$validated['color']);
        } else {
            $cookie = Cookie::get();
            if(empty($cookie['color'])){
                $color = Color::find(1)->firstOrFail();
            } else {
                $color = Color::find((int)$cookie['color']);
            }
        }
        return $color;
    }
}
