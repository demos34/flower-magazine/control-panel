<?php


namespace App\Repositories;


use App\Models\Tags\IndexDescription;
use App\Models\Tags\IndexTag;
use App\Models\Tags\Keyword;
use App\Models\Tags\Tag;
use Illuminate\Http\Request;

class TagsRepository implements TagsRepositoryInterface
{

    public function getAllTags()
    {
        return Tag::all();
    }

    public function getAllIndexTags()
    {
        return IndexTag::all();
    }

    public function getIndexDescription()
    {
        return IndexDescription::find(1)->firstOrFail();
    }

    public function storeTag(Request $request)
    {
        $validated = $this->validateTags($request);
        return Tag::create(
            [
                'bg_tag' => $validated['bg_tag'],
                'eng_tag' => $validated['eng_tag'],
            ]
        );
    }

    public function storeIndexTag(Request $request)
    {
        $validated = $this->validateIndexTags($request);
        return IndexTag::create(
            [
                'bg_tag' => $validated['bg_tag'],
                'eng_tag' => $validated['eng_tag'],
            ]
        );
    }

    public function storeIndexDescription(Request $request)
    {
        //TODO
    }

    public function updateTag(Request $request, Tag $tag)
    {
        $validated = $this->validateTags($request);
        return $tag->update(
            [
                'bg_tag' => $validated['bg_tag'],
                'eng_tag' => $validated['eng_tag'],
            ]
        );
    }

    public function updateIndexTag(Request $request, IndexTag $indexTag)
    {
        $validated = $this->validateIndexTags($request);
        return $indexTag->update(
            [
                'bg_tag' => $validated['bg_tag'],
                'eng_tag' => $validated['eng_tag'],
            ]
        );
    }

    public function updateDescription(Request $request, IndexDescription $indexDescription)
    {
        $validated = $this->validateIndexDescription($request);
        return $indexDescription->update(
            [
                'bg_description' => $validated['bg_description'],
                'eng_description' => $validated['eng_description'],
            ]
        );

    }

    public function deleteTag(Tag $tag)
    {
        return $tag->delete();
    }

    public function deleteIndexTag(IndexTag $indexTag)
    {
        return $indexTag->delete();
    }

    public function getAllKeywords()
    {
        return Keyword::all();
    }

    public function storeKeyword(Request $request)
    {
        $validated = $this->validateKeyword($request);
        return Keyword::create(
            [
                'bg_keyword' => $validated['bg_keyword'],
                'eng_keyword' => $validated['eng_keyword'],
            ]
        );
    }

    public function updateKeyword(Request $request, Keyword $keyword)
    {
        $validated = $this->validateKeyword($request);
        return $keyword->update(
            [
                'bg_keyword' => $validated['bg_keyword'],
                'eng_keyword' => $validated['eng_keyword'],
            ]
        );

    }

    public function deleteKeyword(Keyword $keyword)
    {
        return $keyword->delete();
    }

    private function validateTags(Request $request)
    {

        return $request->validate(
            [
                'bg_tag' => ['required', 'string', 'min:3', 'max:255', 'unique:tags'],
                'eng_tag' => ['required', 'string', 'min:3', 'max:255', 'unique:tags'],
            ]
        );
    }

    private function validateIndexTags(Request $request)
    {

        return $request->validate(
            [
                'bg_tag' => ['required', 'string', 'min:3', 'max:255', 'unique:index_tags'],
                'eng_tag' => ['required', 'string', 'min:3', 'max:255', 'unique:index_tags'],
            ]
        );
    }

    private function validateIndexDescription(Request $request)
    {

        return $request->validate(
            [
                'bg_description' => ['required', 'string', 'min:3', 'max:255', 'unique:index_descriptions'],
                'eng_description' => ['required', 'string', 'min:3', 'max:255', 'unique:index_descriptions'],
            ]
        );
    }

    private function validateKeyword(Request $request)
    {

        return $request->validate(
            [
                'bg_keyword' => ['required', 'string', 'min:3', 'max:255', 'unique:keywords'],
                'eng_keyword' => ['required', 'string', 'min:3', 'max:255', 'unique:keywords'],
            ]
        );
    }


}
