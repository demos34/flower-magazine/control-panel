@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header" align="center">
                        Лични съобщения
                    </div>
                </div>
                @foreach($messages as $msg)
                    <div class="card my-5" @if($msg->isRead === 0)style="background-color: yellowgreen"@endif>
                        <div class="card-header text-center">
                            {{$msg->from}}
                            <br>
                            <small>{{$msg->created_at}}</small>
                        </div>
                        <div class="card-body">
                            <p>
                                {{$msg->msg}}

                                {{$msg->update(['isRead'=>true])}}
                            </p>
                        </div>
                        <div class="card-footer text-center">
                            <a href="/messages/{{$msg->id}}">
                                <button type="button" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete this message</button>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
