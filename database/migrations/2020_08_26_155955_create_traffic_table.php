<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrafficTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traffic', function (Blueprint $table) {
            $table->id();
            $table->string('username')->nullable();
            $table->string('user_id')->nullable();
            $table->string('user_role')->nullable();
            $table->string('remote_address')->nullable();
            $table->text('user_agent')->nullable();
            $table->longText('http_cookie')->nullable();
            $table->string('previous_url')->nullable();
            $table->string('method')->nullable();
            $table->string('request_uri')->nullable();
            $table->string('directory')->nullable();
            $table->string('class')->nullable();
            $table->string('action')->nullable();
            $table->string('given_id_in_action')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traffic');
    }
}
