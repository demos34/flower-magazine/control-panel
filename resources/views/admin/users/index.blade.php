@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 my-2">
                <div class="py-2">
                    <a href="/administrator/" class="float-left">
                        <button class="btn btn-outline-primary">Back</button>
                    </a>
                    <a class="float-right" href="/createNew/">
                        <button class="btn btn-outline-success">{{ __('Register') }}</button>
                    </a>
                </div>
            </div>
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header" align="center">
                        Потребители в контролния панел:
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>E-mail</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->username}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{implode(',', $user->roles()->get()->pluck('role')->toArray())}}</td>
                                        <td>
                                            <a href="/users/{{$user->id}}" class="float-left">
                                                <button class="btn btn-outline-success">View</button>
                                            </a>
                                            <form action="/users/{{$user->id}}" method="POST" class="float-right">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-outline-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
