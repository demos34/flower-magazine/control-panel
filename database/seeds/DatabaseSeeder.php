<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TrafficTableTruncateSeeder::class);
        $this->call(HomeSeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(TagTableSeeder::class);
        $this->call(KeywordTableSeeder::class);
        $this->call(SortItemsTableSeeder::class);
    }
}
