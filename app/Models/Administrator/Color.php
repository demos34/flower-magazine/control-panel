<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->hasMany('App\Models\Products\Product');
    }
}
