<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->belongsToMany('App\Models\Products\Product');
    }
}
