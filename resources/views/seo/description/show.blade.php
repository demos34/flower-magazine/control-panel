@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div>
                    <a href="/seo/">
                        <button class="btn btn-outline-dark">Back</button>
                    </a>
                </div>
                <div class="card my-3 mx-auto">
                    <div class="card-header">
                        <h4 align="center">
                            Мета-таг за продукти <strong style="color: blue">{{$description->bg_description}}
                                / {{$description->eng_description}}</strong>, използван в следните продукти:
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="card-group">
                            <div class="mx-5">#</div>
                            <div class="mx-5">Product type</div>
                            <div class="mx-auto">Product</div>
                        </div>
                        <hr>
                        <div class="card-group">
                            <div class="mx-5">ID</div>
                            <div class="mx-4">
                                <a href="#">Опаковки / Цветя</a>
                            </div>
                            <div class="mx-auto">
                                <a href="#">Продукт(опаковка/цвете)</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-3">
                <div class="well">
                    <h4 align="center">
                        <label>
                            Добави нов мета-таг
                        </label>
                    </h4>
                    <form action="/description/{{$description->id}}/" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="card">
                            <div class="card-header">
                                <label for="bg_description">
                                    Bg description:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="bg_description"
                                           type="text"
                                           class="form-control @error('bg_description') is-invalid @enderror"
                                           name="bg_description"
                                           value=@if(old('bg_description'))"{{ old('bg_description') }}"@else
                                        "{{$description->bg_description}}"@endif
                                    required autocomplete="bg_description" autofocus>
                                    @error('bg_description')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <div class="card-header">
                                <label for="eng_description">
                                    Eng description:
                                </label>
                            </div>
                            <div class="card-body">
                                <h5>
                                    <input id="eng_description"
                                           type="text"
                                           class="form-control @error('eng_description') is-invalid @enderror"
                                           name="eng_description"
                                           value=@if(old('eng_description'))"{{ old('eng_description') }}"@else
                                        "{{$description->eng_description}}"@endif
                                    required autocomplete="eng_description" autofocus>
                                    @error('eng_description')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
